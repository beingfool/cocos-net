import Singleton from "../base/Singleton";
import { STORAGE } from "../config/Enums";
import { DB_INFO_INTER, SEARCH_HISTORY_INTER } from "../config/Interface";

export default class GameData extends Singleton {

    private _recordDatas: any = {};
    set recordDatas(data: any) {
        this._recordDatas = data;
        app.utils.setItem(STORAGE.RECORD_DATAS, data);
    }

    get recordDatas(): any {
        return this._recordDatas;
    }
    
    
    private _searchHistory: SEARCH_HISTORY_INTER[] = [];
    set searchHistory(list: SEARCH_HISTORY_INTER[]) {
        this._searchHistory = list;
        app.utils.setItem(STORAGE.HISTORY_SEARCH, list);
    }

    get searchHistory(): SEARCH_HISTORY_INTER[] {
        return this._searchHistory;
    }

    
    /**************** json ****************/
    private _dbJson: any = {};
    set dbJson(data: Record<string, Record<string, DB_INFO_INTER[]>>) {
        this._dbJson = data;
    }

    get dbJson(): Record<string, Record<string, DB_INFO_INTER[]>> {
        return this._dbJson;
    }

}