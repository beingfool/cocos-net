import Singleton from "../base/Singleton";

export default class PoolMgr extends Singleton {

    private gamePool: Record<string, cc.NodePool> = {}
    private prefabCaches: Record<string, cc.Prefab> = {}

    async initPool(name: string, url?: string, prefab?: cc.Prefab) {
        if (this.gamePool[name]) {
            // console.error('PoolMgr initPool is create!');
            return;
        }
        this.gamePool[name] = new cc.NodePool();
        if (prefab) {
            this.prefabCaches[name] = prefab;
        } else if (url) {
            this.prefabCaches[name] = await app.resMgr.getPrefab(url);
        } else {
            console.warn('PoolMgr url and prefab is null!', name);
        }
    }

    get(name: string, prefab?: cc.Prefab, func?: () => cc.Node): cc.Node {
        if (this.gamePool[name] == null) {
            this.initPool(name);
        }
        if (this.gamePool[name].size() > 0) {
            return this.gamePool[name].get();
        } else if (this.prefabCaches[name]) {
            return cc.instantiate(this.prefabCaches[name]);
        }
        if (prefab) {
            this.prefabCaches[name] = prefab;
            return cc.instantiate(this.prefabCaches[name]);
        } else {
            return func && func();
        }
    }

    put(name: string, node: cc.Node) {
        if (this.gamePool[name] == null) {
            this.initPool(name);
        }
        this.gamePool[name].put(node);
    }

}