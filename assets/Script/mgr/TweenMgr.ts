import Singleton from "../base/Singleton";
import { ACTION_TYPE } from "../config/Enums";
import { ACT_CFG_INTER } from "../config/Interface";

export default class TweenMgr extends Singleton {

    /**
     * actionFloat的缓动版本
     * @param from 开始值
     * @param to 结束值
     * @param onUpdate 每帧回调
     * @param onComplete 完成回调
     */
    public tweenFloat(from: number, to: number, duration: number, onUpdate: (t: number, dtTime?: number, dtNum?: number) => void, onComplete?: Function, autoStart: boolean = true): cc.Tween {
        let o: Record<string, number> = { _value: from };
        let time = Date.now();
        let last = from;
        Object.defineProperty(o, 'value', {
            get: () => o._value,
            set: (v: number) => {
                o._value = v;
                let now = Date.now();
                let dt = (now - time) / 1000;
                time = now;
                let dtNum = v - last
                last = v;
                onUpdate && onUpdate(o._value, dt, dtNum);
            },
        });
        let tween = cc.tween(o).to(duration, { value: to }).call(onComplete);
        if (autoStart) {
            tween.start();
        }
        return tween;
    }

    public getTweenByType(actType: ACTION_TYPE, node: cc.Node, config: ACT_CFG_INTER): cc.Tween {
        let time = config.time || 0.5;
        let delayTime = config.delayTime || 0;
        let doTimes = config.doTimes || 1;
        let value = config.value || 0.01;
        let movePos = config.pos || cc.v2(0, 0);
        let pos = node.position;
        switch (actType) {
            case ACTION_TYPE.ROTATE:
                return cc.tween().delay(delayTime).by(time, { angle: -360 });
            case ACTION_TYPE.BREATH:
                let scale = node.scale;
                return cc.tween().delay(delayTime).to(time, { scale: scale * (1 + value) }).to(time, { scale: scale * (1 - value) });
            case ACTION_TYPE.TWINKLE:
                return cc.tween().delay(delayTime).to(time, { opacity: value }).to(time, { opacity: 255 });
            case ACTION_TYPE.UP:
                return cc.tween().delay(delayTime).to(time, { y: pos.y + value }).to(time, { y: pos.y });
            case ACTION_TYPE.TREMBLE:
                let tween1 = { x: pos.x + movePos.x, y: pos.y + movePos.y };
                let tween2 = { x: pos.x - movePos.x, y: pos.y - movePos.y };
                return cc.tween().delay(delayTime).to(time, tween1).to(time, tween2);
            case ACTION_TYPE.ROCK:
                let angle = node.angle;
                if (doTimes > 1) {
                    return cc.tween().delay(delayTime).to(time / 2, { angle: angle + value }).repeat(doTimes - 1, cc.tween().to(time, { angle: angle - value }).to(time, { angle: angle + value })).to(time / 2, { angle: angle });
                } else {
                    return cc.tween().delay(delayTime).to(time / 2, { angle: angle + value }).to(time, { angle: angle - value }).to(time / 2, { angle: angle });
                };
            case ACTION_TYPE.MOVE:
                return cc.tween().delay(delayTime).to(time, { x: pos.x + movePos.x, y: pos.y + movePos.y }).to(time, { x: pos.x, y: pos.y });
        }
    }

    public shake(node: cc.Node) {
        let tween = cc.tween()
            .to(0.15, { angle: -9 })
            .to(0.15, { angle: 0 })
            .to(0.15, { angle: 9 })
            .to(0.15, { angle: 0 })
            .union()
            .repeatForever()
        let tween2 = cc.tween(node)
            .to(0.4, { scale: 0.8 })
            .to(0.4, { scale: 1 })
            .to(0.4, { scale: 1.2 })
            .to(0.4, { scale: 1 })
            .union()
            .repeatForever();
        return cc.tween(node)
            .parallel(
                tween,
                tween2
            ).start()
    }

    private tweenNode: cc.Node;
    public doDiffListAct(list: cc.Tween[], callack?: Function) {
        if (!this.tweenNode) {
            this.tweenNode = new cc.Node();
            cc.game.addPersistRootNode(this.tweenNode)
        }
        let curIdx: number = -1;
        let doAct = async (idx: number) => {
            if (curIdx >= idx) return;
            curIdx = idx;

            if (list && list[idx]) {
                let tween = list[idx].call(() => {
                    doAct(idx + 1);
                }).start();
                let time = tween["_finalAction"]?._duration || 0;
                if (time < 1 / 60) {
                    time = 0;
                }
                cc.tween(this.tweenNode).delay(time).call(() => {
                    if (tween["_target"] && tween["_target"].active == false) {
                        doAct(idx + 1);
                    }
                }).start()
            } else {
                callack && callack();
            }
        }
        doAct(0);
    }

    public addNumAct(lab: cc.Label, start: number, end: number, time: number = 1, startStr: string = ``, endStr: string = ``) {
        if (!lab) {
            console.error('TweenMgr addNumAct');
            return;
        }
        this.tweenFloat(start, end, time, (num: number) => {
            lab.string = `${startStr}${Math.floor(num)}${endStr}`;
        });
    }

    /**
     * @desc 贝塞尔
     * @param {number} t 当前百分比
     * @param {cc.Vec2} p1 起点坐标
     * @param {cc.Vec2} p2 终点坐标
     * @param {cc.Vec2[]} cps 控制点
     */
    public pointBezier(t: number, p1: cc.Vec2, p2: cc.Vec2, cps?: cc.Vec2[]): cc.Vec2 {
        if (cps == null || cps.length == 0) {
            let x = p1.x + (p2.x - p1.x) * t;
            let y = p1.y + (p2.y - p1.y) * t;
            return cc.v2(x, y);
        }
        if (cps.length == 1) {
            let x = (1 - t) * (1 - t) * p1.x + 2 * t * (1 - t) * cps[0].x + t * t * p2.x;
            let y = (1 - t) * (1 - t) * p1.y + 2 * t * (1 - t) * cps[0].y + t * t * p2.y;
            return cc.v2(x, y);
        }
        if (cps.length == 2) {
            let x =
                p1.x * (1 - t) * (1 - t) * (1 - t) +
                3 * cps[0].x * t * (1 - t) * (1 - t) +
                3 * cps[1].x * t * t * (1 - t) +
                p2.x * t * t * t;
            let y =
                p1.y * (1 - t) * (1 - t) * (1 - t) +
                3 * cps[0].y * t * (1 - t) * (1 - t) +
                3 * cps[1].y * t * t * (1 - t) +
                p2.y * t * t * t;
            return cc.v2(x, y);
        }
    }

    public getMoveTime(starPos: cc.Vec2, endPos: cc.Vec2, moveNum: number = 500): number {
        let len = Math.sqrt(Math.pow((starPos.x - endPos.x), 2) + Math.pow((starPos.y - endPos.y), 2));
        return len / moveNum;
    }


    /**
     * @param k 当前事件与总耗时的百分比
     * @returns 
     */
    cubicOut(k: number) {
        return --k * k * k + 1;
    }

    backInOut(k: number) {
        var s = 1.70158 * 1.525;
        if ((k *= 2) < 1) {
            return 0.5 * (k * k * ((s + 1) * k - s));
        }
        return 0.5 * ((k -= 2) * k * ((s + 1) * k + s) + 2);
    }

    backOut(k) {
        var s = 1.70158;
        return --k * k * ((s + 1) * k + s) + 1;
    }
}