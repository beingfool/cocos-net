import BaseUI from "../base/BaseUI";
import Singleton from "../base/Singleton";
import Tips from "../com/Tips";
import { VIEW_TYPE } from "../config/Enums";
import { UI_INTER, VIEW_INTER } from "../config/Interface";
import { UI, UI_RES } from "../config/MainConfig";

export default class ViewMgr extends Singleton {

    private list: Record<string, UI_INTER> = {};
    private tips: Tips = null;
    private uiLayer: cc.Node = null;
    private dialogLayer: cc.Node = null;
    private tipsLayer: cc.Node = null;

    public initLayer(uiLayer: cc.Node, dialogLayer: cc.Node, tipsLayer: cc.Node) {
        this.uiLayer = uiLayer;
        this.dialogLayer = dialogLayer;
        this.tipsLayer = tipsLayer;
    }

    /**
     * 提示
     * @param content 内容
     * @param position 位置
     */
    public async showTips(content: string, time: number = 1.5, position?: cc.Vec2) {
        if (this.tips && cc.isValid(this.tips)) {
            this.tips.show(content, {position: position, time: time });
        } else {
            const url: string = app.main.prefab.Tips;
            const prefab: cc.Prefab = await app.resMgr.getPrefab(url);
            if (prefab) {
                this.createTips(prefab, content, position, time);
            } else {
                app.resMgr.loadRes({url:url, type:cc.Prefab}, cc.Prefab, false, (error, prefab) => {
                    !error && this.createTips(prefab, content, position, time);
                });
            }
        }
    }

    private waitMap = { count: 0 };
    public addWait(waitStr: string = "wait", arg?: string) {
        this.waitMap.count++;
        if (!this.waitMap[waitStr]) {
            this.waitMap[waitStr] = {};
        }
        let name = arg || "times";
        let times = this.waitMap[waitStr][name] || 0;
        this.waitMap[waitStr][name] = times + 1;
        this.showUI(UI.WAIT);
    }

    public delWait(waitStr: string = "wait", arg?: string) {
        this.waitMap.count--;
        if (this.waitMap.count <= 0) {
            this.waitMap.count = 0;
            this.hideUI(UI.WAIT);
        }
        if (this.waitMap[waitStr]) {
            let name = arg || "times";
            let times = this.waitMap[waitStr][name] || 0;
            this.waitMap[waitStr][name] = times - 1;
            if (this.waitMap[waitStr][name] <= 0) {
                delete this.waitMap[waitStr][name];
            }
        }
    }

    public clearWait() {
        this.waitMap = { count: 0 };
        this.hideUI(UI.WAIT);
    }

    public logWaitInfo() {
        console.log('wait info:', JSON.stringify(this.waitMap));
    }
    
    /**
     * 隐藏UI
     * @param name 名称
     */
    public hideUI(name: string, isDestroy: boolean = false): void {
        // console.log('ViewMgr hideUI', name);
        const uiData: UI_INTER = this.list[name];
        if (uiData && uiData.ui && cc.isValid(uiData.ui)) {
            let viewCfg:VIEW_INTER = UI_RES[name];
            let tsName = viewCfg.tsName || "BaseUI";
            let comJs:BaseUI = uiData.ui.getComponent(tsName);
            if (comJs) {
                comJs.close && comJs.close();
            }
            uiData.ui.active = false;
            let destroy = isDestroy || viewCfg.isDestroy;
            if (destroy) {
                delete this.list[name];
                uiData.ui.destroy();
            }
            app.event.emit(app.eventName.HIDE_VIEW, name);
        }
    }

    /**
     * 显示ui
     * @param name 名称
     * @param active 是否显示
     */
    public async showUI(name: string, arg?: any, callBack?: Function): Promise<void> {
        // console.log('ViewMgr showUI', name);
        const viewCfg: VIEW_INTER = UI_RES[name];
        const uiData: UI_INTER = this.list[name];
        if(uiData && uiData.isLoading){
            console.warn('ViewMgr showUI loading', name);
            return;
        }
        if (uiData && uiData.ui && cc.isValid(uiData.ui)) {
            this.showTop(uiData)
            if(uiData.ui.active) return;
            this.postArg(name, uiData.ui, arg);
            uiData.ui.active = true;
            callBack && callBack();
        } else {
            if (name != UI.WAIT) {
                this.addWait("showUI", name);
            }
            this.list[name] = {isLoading:true, ui:null, viewType:null}
            const prefab: cc.Prefab = await app.resMgr.getPrefab(viewCfg.url, viewCfg.bundleName);
            if (prefab) {
                this.createUI(name, prefab, viewCfg, arg, callBack);
            } else {
                app.resMgr.loadRes(viewCfg, cc.Prefab, false, (err, res) => {
                    this.createUI(name, res, viewCfg, arg, callBack);
                });
            }
        }
    }

    showTop(uiData: UI_INTER) {
        let zIndex = uiData.ui.zIndex;
        let viewType = uiData.viewType;
        for (const name in this.list) {
            const data = this.list[name];
            if (data && data.ui && cc.isValid(data.ui) && data.ui.active && data.viewType == viewType && zIndex < data.ui.zIndex) {
                zIndex = this.uiZIndex++;
            }
        }
        if (zIndex > uiData.ui.zIndex) {
            uiData.ui.zIndex = zIndex;
        }
    }

    private uiZIndex = 0;
    private createUI(name: string, prefab: cc.Prefab, viewCfg?: any, arg?: any, callBack?: Function): void {
        if (prefab == null) {
            if (name != UI.WAIT) {
                this.delWait("showUI", name);
            }
            delete this.list[name];
            console.error('show UI', name);
            return;
        }
        const ui: cc.Node = cc.instantiate(prefab);
        let sceneName = cc.director.getScene().name;
        if (viewCfg && viewCfg.viewType == VIEW_TYPE.TOP) {
            this.addToLayer(ui, cc.find('Canvas'));
            ui.zIndex = 99;
        } else if (viewCfg && viewCfg.viewType == VIEW_TYPE.TIPS) {
            this.addToLayer(ui, this.tipsLayer);
        } else if (viewCfg && viewCfg.viewType == VIEW_TYPE.DIALOG) {
            this.addToLayer(ui, this.dialogLayer);
        } else {
            this.addToLayer(ui, this.uiLayer);
        }
        this.list[name] = { ui: ui, viewType: viewCfg.viewType, scene:sceneName};
        ui.zIndex = this.uiZIndex++;
        ui.active = true;
        this.postArg(name, ui, arg);
        callBack && callBack();
        if(name != UI.WAIT){
            this.delWait("showUI", name);
        }
        app.event.emit(app.eventName.SHOW_VIEW, name);
    }

    private postArg(name: string, ui: cc.Node, arg?: any) {
        let tsName = UI_RES[name].tsName || "BaseUI";
        let comJs:BaseUI = ui.getComponent(tsName);
        if (comJs) {
            comJs.setArg && comJs.setArg(arg);
            comJs.open && comJs.open();
        }
    }

    private createTips(prefab: cc.Prefab, content: string, position: cc.Vec2, time: number): void {
        const node: cc.Node = cc.instantiate(prefab);
        this.addToLayer(node, this.tipsLayer);
        node.zIndex = 999;
        this.tips = node.getComponent(Tips);
        this.tips.show(content, {position: position, time: time });
    }

    private addToLayer(ui: cc.Node, layer?: cc.Node) {
        if (!ui) {
            console.error('addToLayer ui == null');
            return false;
        }
        if (!layer || !cc.isValid(layer)) {
            ui.parent = cc.find('Canvas');
        } else {
            ui.parent = layer;
        }
        return true;
    }
}