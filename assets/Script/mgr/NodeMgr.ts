import Singleton from "../base/Singleton";
import { CLICK_ARG_INTER } from "../config/Interface";

export default class NodeMgr extends Singleton {
    private normal: cc.Material = null;
    private gray: cc.Material = null;

    constructor() {
        super();
        this.normal = cc.Material.getBuiltinMaterial("" + cc.Material.BUILTIN_NAME.SPRITE)
        this.gray = cc.Material.getBuiltinMaterial("" + cc.Material.BUILTIN_NAME.GRAY_SPRITE)
    }

    public addTouch(node: cc.Node, config: { touchStart?: Function, touchMove?: Function, touchEnd?: Function, touchCancel?: Function, target?: any }) {
        node.on(cc.Node.EventType.TOUCH_START, config.touchStart, config.target);
        node.on(cc.Node.EventType.TOUCH_MOVE, config.touchMove, config.target);
        node.on(cc.Node.EventType.TOUCH_END, config.touchEnd, config.target);
        node.on(cc.Node.EventType.TOUCH_CANCEL, config.touchCancel, config.target);
    }

    public removeTouch(node: cc.Node, config: { touchStart?: Function, touchMove?: Function, touchEnd?: Function, touchCancel?: Function, target?: any }) {
        if (!cc.isValid(node)) return;
        node.off(cc.Node.EventType.TOUCH_START, config.touchStart, config.target);
        node.off(cc.Node.EventType.TOUCH_MOVE, config.touchMove, config.target);
        node.off(cc.Node.EventType.TOUCH_END, config.touchEnd, config.target);
        node.off(cc.Node.EventType.TOUCH_CANCEL, config.touchCancel, config.target);
    }

    public getNodeInNodePos(node1: cc.Node, node2: cc.Node) {
        let wPos = node1.convertToWorldSpaceAR(cc.v2());
        let nPos = node2.convertToNodeSpaceAR(wPos);
        return nPos;
    }


    public addListItem(len: number, list: cc.Node[], template: cc.Prefab | cc.Node, parent: cc.Node, callback: (item: cc.Node, idx: number, isNew: boolean) => void) {
        this.addListItemFunc(len, list, parent, () => {
            return cc.instantiate(template) as cc.Node;
        }, callback)
    }

    public addListItemSelf(len: number, list: cc.Node[], template: cc.Node, parent: cc.Node, callback: (item: cc.Node, idx: number, isNew: boolean) => void) {
        this.addListItemFunc(len, list, parent, (idx: number) => {
            if (idx == 0) {
                return template;
            }
            return cc.instantiate(template);
        }, callback)
    }

    public addListItemFunc(len: number, list: cc.Node[], parent: cc.Node, createCb: (idx: number) => cc.Node, callback: (item: cc.Node, idx: number, isNew: boolean) => void) {
        if (parent == null) return;
        list = list || [];
        for (let i = 0; i < len; i++) {
            let item: cc.Node = list[i];
            let isNew = false
            if (!item) {
                if (createCb) {
                    item = createCb(i);
                } else {
                    item = new cc.Node(`item${i}`);
                }
                item.parent = parent;
                list[i] = item;
                isNew = true;
            }
            item.active = true;
            callback && callback(item, i, isNew);
        }
        for (let i = len; i < list.length; i++) {
            list[i].active = false;
        }
    }

    /**
     * 把背景,整体放大,顶满屏幕
     * @param node 
     */
    public adaptNode2MinScreen(node: cc.Node, adaptSize: boolean = false) {
        if (!node) {
            console.warn('adaptNode2MinScreen node == null');
            return
        }
        let scale = 1;
        let radio1 = cc.winSize.width / cc.winSize.height
        let radio2 = node.width / node.height
        let isWidth = false
        if (radio1 > radio2) {
            scale = cc.winSize.height / node.height
        } else {
            scale = cc.winSize.width / node.width
            isWidth = true;
        }
        node.scale = scale;
        if (adaptSize) {
            if (isWidth) {
                node.height = node.width / radio1;
            } else {
                node.width = node.height * radio1;
            }
        }
    }

    public getAdaptMinScale(node?: cc.Node, width: number = 750, height: number = 1334): number {
        let scale = 1;
        if (node) {
            width = node.width;
            height = node.height;
        }
        if (width && height) {
            let radio1 = cc.winSize.width / cc.winSize.height
            let radio2 = width / height
            if (radio1 > radio2) {
                scale = cc.winSize.height / height
            } else {
                scale = cc.winSize.width / width
            }
        }
        return scale;
    }

    /**
     * 把节点的size设置成屏幕大小
     * @param node 
     * @param arg 
     */
    public adaptSize2Winsize(node: cc.Node) {
        if (!node) {
            console.warn('adaptSize2Winsize 15 node == null');
            return
        }
        node.height = cc.winSize.height
        node.width = cc.winSize.width
    }

    public setGray(node: cc.Node, isGray: boolean) {
        let spr: cc.Sprite = node.getComponent(cc.Sprite);
        if (spr) {
            spr.setMaterial(0, isGray ? this.gray : this.normal);
        }
        let lab: cc.Label = node.getComponent(cc.Label);
        if (lab) {
            lab.setMaterial(0, isGray ? this.gray : this.normal);
        }
    }

    public setAllGray(node: cc.Node, gray: boolean) {
        if (node instanceof cc.Node) {
            this.setGray(node, gray)
            node.children.forEach(temp => {
                this.setGray(temp, gray)
            })
        }
    }

    public setAnimation(spine: sp.Skeleton, param: { act: string, loop?: boolean, timeScale?: number, complete?: () => void, frame?: () => void }) {
        let { act = "", loop = false, timeScale = 1, complete = null, frame = null } = param;

        if (!act || !spine || !spine.skeletonData) {
            // this.simulateAnimation(complete, frame)
            return
        }
        if (act != spine.animation) {
            spine.setAnimation(0, act, loop);
        }
        spine.timeScale = timeScale;
        spine.setCompleteListener(null);
        spine.setEventListener(null);
        spine.setCompleteListener(() => {
            if (!loop) {
                spine.timeScale = 1;
                spine.setCompleteListener(null);
                spine.setEventListener(null);
            }
            complete && complete();
        });
        spine.setEventListener((trackEntry: sp.spine.TrackEntry, event) => {
            frame && frame();
        });
    }

    public playSpineAnimation(spine: sp.Skeleton, animationName: string, isLoop: boolean = false, callback?: Function) {
        spine.setAnimation(0, animationName, isLoop);
        let trackIndex = spine.getCurrent(0);
        spine.setTrackCompleteListener(trackIndex, () => {
            callback && callback();
        });
    }

    /**
     * 按钮点击事件
     * @param button 按钮
     * @param callback 回调函数(tagert: cc.Button)
     * @param target this对象
     */
    public onClick(button: cc.Node | cc.Button, callback: Function, target?: any, data?: CLICK_ARG_INTER): void {
        if (button == null) {
            console.error('没有这节点');
            return
        }
        let btnNode: cc.Node = null;
        if (button instanceof cc.Button) {
            btnNode = button.node;
        } else if (button instanceof cc.Node) {
            btnNode = button;
        }
        let canClick = true;
        let onClickReal = function (self) {
            if (!canClick) return;
            canClick = false;
            data = data || {};
            let clickTime = data.clickTime == null ? 0.1 : data.clickTime;
            self.scheduleOnce(() => {
                canClick = true;
            }, clickTime);
            // if (!data || !data.noSound) {
            //     if (data && data.effect) {
            //         app.audioMgr.playSound(data.effect, false, null, data.boundle || BUNDLENAME.RESOURCES);
            //     } else {
            //         app.audioMgr.playSound(app.main.audio.click, false, null, BUNDLENAME.RESOURCES);
            //     }
            // }
            callback.call(target)
        }
        this.offClick(button)
        if (btnNode != null) {
            btnNode.targetOff(this)
            btnNode.on(app.eventName.CLICK, onClickReal, this);
        }
    }

    /**
     * 注销按钮点击事件
     * @param button 按钮
     * @param callback 回调函数(tagert: cc.Button)
     * @param target this对象
     */
    public offClick(button: cc.Node | cc.Button, callback?: Function, target?: any): void {
        let btnNode: cc.Node = null;
        if (button instanceof cc.Button) {
            btnNode = button.node;
        } else if (button instanceof cc.Node) {
            btnNode = button;
        }
        btnNode.off(app.eventName.CLICK, callback, target);
    }
}