
// 提醒管理

import Singleton from "../base/Singleton";
import { REMIND_NAME } from "../config/Enums";

export default class RemindMgr extends Singleton {

    private checkRemindFunc = {};
    private remindList = {};

    constructor() {
        super();
        this.initData();
    }

    initData() {
        this.checkRemindFunc = {};
        this.remindList = {};
    }

    // 注册提醒数量计算函数
    registerCheckRemind(name: string, func: Function, target: any) {
        this.checkRemindFunc[name] = {}
        this.checkRemindFunc[name].func = func
        this.checkRemindFunc[name].target = target

        this.remindList[name] = func.call(target);

        this.doRemind(name);
    }

    getRemind(name: REMIND_NAME | string) {
        return this.remindList[name] || 0
    }

    //刷新某提醒
    doRemind(name: string) {
        if (this.checkRemindFunc[name]) {
            let num = this.checkRemindFunc[name].func.call(this.checkRemindFunc[name].target);
            if (num != this.getRemind(name)) {
                this.remindList[name] = num;
                app.event.emit(app.eventName.REMIND_CHANGE, name);
            }
        } else {
            console.error("RemindManager 没有注册该红点事件:" + name);
        }
    }
}


