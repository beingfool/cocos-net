import Singleton from "../base/Singleton";
import { BUNDLENAME } from "../config/Enums";
import { RES_INTER, SPRITE_ADAPT_INTER } from "../config/Interface";

const { ccclass, property } = cc._decorator;
@ccclass
export default class SpriteMgr extends Singleton {
    private spriteCaches = {};//存储了图片

    public addSpriteToCaches(url: string, sprite: cc.SpriteFrame): void {//从路径中拿到图片
        //console.log("添加东西",url,prefab);
        if (url && sprite) {
            this.spriteCaches[url] = sprite;
        }
    };

    public loadNetImage(sp: cc.Sprite, url: string, isExp: boolean = false): void {//下载图片
        if (url == null || url == "") {
            return;
        }
        var size = { width: sp.node.width, height: sp.node.height };
        app.resMgr.loadRemoteWithSuffix<cc.Texture2D>(url, { ext: '.png' }, (err, texture) => {
            if (err) {
                return;
            }
            //console.log("才下载下来",url);
            var frame = new cc.SpriteFrame(texture);
            sp.spriteFrame = frame
            sp.node.width = size.width;
            sp.node.height = size.height;
            if (isExp) {
                this.addSpriteToCaches(url, frame);
            }
        });
    };

    public async loadImg(spr: cc.Sprite | cc.Node, url: string, bundleName?: BUNDLENAME, scaleCfg?: SPRITE_ADAPT_INTER, loadCB?: Function) {
        let sprite: cc.Sprite = null
        if (spr instanceof cc.Node) {
            sprite = spr.getComponent(cc.Sprite);
        } else {
            sprite = spr;
        }
        if (!sprite) {
            return;
        }
        let sprf = await app.resMgr.getSpriteFrame(url, bundleName);
        if (sprf) {
            sprite.spriteFrame = sprf;
            if (scaleCfg) {
                this.adaptSpriteSize(sprite.node, scaleCfg);
            }
            loadCB && loadCB();
            return;
        }
        let res: string | RES_INTER;
        if (bundleName != null) {
            res = {
                url: url,
                type: cc.SpriteFrame,
                bundleName: bundleName,
            }
        } else {
            res = url;
        }
        app.resMgr.loadRes(res, cc.SpriteFrame, true, (err, asset) => {
            if (err) {
                console.error("loadImg url : ", res, err);
                return;
            }
            sprite.spriteFrame = asset;
            if (scaleCfg) {
                this.adaptSpriteSize(sprite.node, scaleCfg);
            }
            loadCB && loadCB();
        });
    }

    /**
     * 
     * @param node 调整大小的节点
     * @param size 调整的宽高范围
     * @param isScale 是否为缩放调整
     * @returns 
     */
    public adaptNodeSize(node: cc.Node, width?: number, height?: number, isScale: boolean = true): void {
        if (!node) return;
        let wScale: number;
        let hScale: number;
        let realScale: number;
        if (width) {
            wScale = width / node.width;
            realScale = wScale;
        }
        if (height) {
            hScale = height / node.height;
            realScale = hScale;
        }
        if (wScale != null && hScale != null) {
            if (isScale) {
                node.setScale(wScale, hScale);
            } else {
                node.setContentSize(node.width * wScale, node.height * hScale);
            }
        } else if (realScale != null) {
            if (isScale) {
                node.scale = realScale;
            } else {
                node.setContentSize(node.width * realScale, node.height * realScale);
            }
        }
    }

    public adaptSpriteSize(node: cc.Node, config: SPRITE_ADAPT_INTER) {
        if (!node || !config) return;
        let wScale: number;
        let hScale: number;
        let realScale: number;
        if (config.width != null) {
            wScale = config.width / node.width;
        }
        if (config.height != null) {
            hScale = config.height / node.height;
        }
        if (wScale == null || hScale == null) {
            realScale = wScale || hScale || 1;
        } else if (config.isAdaptMax) {
            realScale = Math.max(wScale, hScale);
        } else if (config.isAdaptMin) {
            realScale = Math.min(wScale, hScale);
        }
        if (config.isScale) {
            if (realScale != null) {
                node.scale = realScale;
            } else {
                if (wScale != null) {
                    node.scaleX = wScale;
                }
                if (hScale != null) {
                    node.scaleY = hScale;
                }
            }
        } else {
            if (realScale != null) {
                node.width *= realScale;
                node.height *= realScale;
            } else {
                if (wScale != null) {
                    node.width *= wScale;
                }
                if (hScale != null) {
                    node.height *= hScale;
                }
            }
        }
    }

    public screenshotJsb(target: cc.Node, name?: string, imgType: string = "png"): cc.Texture2D {
        let camera = null;
        var width = Math.floor(target.width);
        var height = Math.floor(target.height);
        let texture = this.getTexture(target, camera);
        let picData = this.renderTex(target, texture, camera);
        this.downloadJsb(`${name}.${imgType}`, picData, width, height);
        return texture;
    }

    public screenshotWeb(target: cc.Node, name: string): cc.Texture2D {
        let camera = null;
        let texture = this.getTexture(target, camera);
        this.downloadWeb(`${name}.png`, texture, camera);
        return texture;
    }

    public getTexture(target: cc.Node, camera: cc.Camera) {
        var width = Math.floor(target.width);
        var height = Math.floor(target.height);
        var cameraNode = new cc.Node();
        cameraNode.parent = target;
        // cameraNode.zIndex = -1;
        camera = cameraNode.addComponent(cc.Camera);
        // camera.cullingMask = 2;
        camera.cullingMask = 0xffffffff;
        camera.alignWithScreen = false;
        camera.ortho = true;
        camera.orthoSize = target.height / 2;
        var texture = new cc.RenderTexture();
        let gl = cc.game['_renderContext'];
        texture.initWithSize(width, height, gl.STENCIL_INDEX8);
        camera.targetTexture = texture;
        return texture;
    }

    public renderTex(target: cc.Node, texture: cc.RenderTexture, camera: cc.Camera): Uint8Array {
        var width = Math.floor(target.width);
        var height = Math.floor(target.height);
        camera.render(null);
        var data = texture.readPixels();
        target.removeChild(camera.node);
        //以下代码将截图后默认倒置的图片回正
        var picData = new Uint8Array(width * height * 4);
        var rowBytes = width * 4;
        for (var row = 0; row < height; row++) {
            var srow = height - 1 - row;
            var start = Math.floor(srow * width * 4);
            var reStart = row * width * 4;
            for (var i = 0; i < rowBytes; i++) {
                picData[reStart + i] = data[start + i];
            }
        }
        let tex = new cc.RenderTexture();
        tex.initWithData(picData, texture.getPixelFormat(), width, height);
        return picData;
    }

    public downloadJsb(fileName: string, picData: Uint8Array, width: number, height: number) {
        // 原生调用
        if (cc.sys.isNative && jsb) {
            // 执行回到
            let wPath = jsb.fileUtils.getWritablePath();
            let fullPath = `${wPath}${fileName}`;
            if (jsb.fileUtils.isDirectoryExist(wPath)) {
                jsb.fileUtils.createDirectory(wPath);
            }
            if (jsb.fileUtils.isFileExist(fullPath)) {
                jsb.fileUtils.removeFile(fullPath);
            }

            // 保存图片
            if (jsb && jsb["saveImageData"] && jsb["saveImageData"](picData, width, height, fullPath) == true) {
                console.log("截图成功", fullPath);
            } else {
                console.log("截图失败", fullPath);
            }
        }
    }

    _canvas = null;
    public downloadWeb(fileName: string, texture: cc.RenderTexture, camera: cc.Camera) {
        var width = texture.width;
        var height = texture.height;
        if (!this._canvas) {
            this._canvas = document.createElement('canvas');
            this._canvas.width = width;
            this._canvas.height = height;
        }
        var ctx = this._canvas.getContext('2d');
        camera.render();
        var data = texture.readPixels();
        // write the render data
        var rowBytes = width * 4;
        for (var row = 0; row < height; row++) {
            var srow = height - 1 - row;
            var imageData = ctx.createImageData(width, 1);
            var start = srow * width * 4;
            for (var i = 0; i < rowBytes; i++) {
                imageData.data[i] = data[start + i];
            }
            ctx.putImageData(imageData, 0, row);
        }

        var dataURL = this._canvas.toDataURL("image/png");
        var img = document.createElement("img");
        img.src = dataURL;
        var aLink = document.createElement('a');
        var parts = img.src.split(';base64,');
        var contentType = parts[0].split(':')[1];
        var raw = window.atob(parts[1]);
        var rawLength = raw.length;
        var uInt8Array = new Uint8Array(rawLength);
        for (var i = 0; i < rawLength; ++i) {
            uInt8Array[i] = raw.charCodeAt(i);
        }
        var blob = new Blob([uInt8Array], { type: contentType });
        var evt = document.createEvent("MouseEvents");
        evt.initEvent("click", false, false);
        aLink.download = fileName;
        aLink.href = URL.createObjectURL(blob);
        aLink.dispatchEvent(evt);
    }
}

