import Singleton from "../base/Singleton";
import { BUNDLENAME } from "../config/Enums";
import { RES_INTER } from "../config/Interface";

export default class ResMgr extends Singleton {

    private bundles: Record<string, RES_INTER> = {};


    public getAsset<T extends cc.Asset>(url: string, type?: typeof cc.Asset, bundleName?: BUNDLENAME | string): Promise<T> {
        return new Promise(async (resolve, reject) => {
            let asset: T = null;
            let bundle = cc.resources;
            if (bundleName && bundleName != "") {
                bundle = await this.getBundle(bundleName, true);
            } else if (this.bundles[url] != null) {
                bundleName = this.bundles[url].bundleName;
                bundle = await this.getBundle(bundleName, true);
            }
            if (bundle) {
                asset = bundle.get(url, type);
                this.loadRemoteWithSuffix
            }
            if (asset == null) {
                asset = await this.loadAsset(url, type, bundleName);
            }
            resolve(asset);
        });
    }

    private loadAsset<T extends cc.Asset>(url: string, type: typeof cc.Asset, bundleName?: string): Promise<T> {
        return new Promise<T>(async (resolve, reject) => {
            let asset: T = null;
            let load = (res: string | RES_INTER, type: typeof cc.Asset) => {
                return new Promise<T>(async (resolve, reject) => {
                    if (typeof (res) != 'string' && res.bundleName) {
                        let bundle = await this.getBundle(res.bundleName, true);
                        asset = bundle.get(url, type)
                        if (asset) {
                            resolve(asset);
                            return;
                        }
                    }

                    this.loadRes(res, type, false, (err1, asset) => {
                        resolve(asset);
                    });
                });
            }
            if (bundleName) {
                asset = await load({ url: url, type: type, bundleName: bundleName }, type);
            }
            if (!asset) {
                asset = await load(url, type);
            }
            resolve(asset);
        });
    }

    /**
     * 加载图集(需要自行加载)
     * @param url 
     */
    public getSpriteAtlas(url: string, bundleName?: BUNDLENAME | string): Promise<cc.SpriteAtlas> {
        return this.getAsset<cc.SpriteAtlas>(url, cc.SpriteAtlas, bundleName);
    }

    /**
     * 加载预设体(需要自行加载)
     * @param url 
     */
    public getPrefab(url: string, bundleName?: BUNDLENAME | string): Promise<cc.Prefab> {
        return this.getAsset<cc.Prefab>(url, cc.Prefab, bundleName);
    }

    /**
     * 加载json(需要自行加载)
     * @param url 
     */
    public getJsonAsset(url: string, bundleName?: BUNDLENAME | string): Promise<cc.JsonAsset> {
        return this.getAsset<cc.JsonAsset>(url, cc.JsonAsset, bundleName);
    }

    /**
     * 加载audio(需要自行加载)
     * @param url 
     */
    public getAudioClip(url: string, bundleName?: BUNDLENAME | string): Promise<cc.AudioClip> {
        return this.getAsset<cc.AudioClip>(url, cc.AudioClip, bundleName);
    }

    /**
     * 加载精灵帧(需要自行加载)
     * @param url 
     */
    public getSpriteFrame(url: string, bundleName?: BUNDLENAME | string): Promise<cc.SpriteFrame> {
        return this.getAsset<cc.SpriteFrame>(url, cc.SpriteFrame, bundleName);
    }

    /**
     * 加载Texture2D(需要自行加载)
     * @param url 
     */
    public getTexture2D(url: string, bundleName?: BUNDLENAME | string): Promise<cc.Texture2D> {
        return this.getAsset<cc.Texture2D>(url, cc.Texture2D, bundleName);
    }

    /**
     * 加载font(需要自行加载)
     * @param url 
     */
    public getFont(url: string, bundleName?: BUNDLENAME | string): Promise<cc.Font> {
        return this.getAsset<cc.Font>(url, cc.Font, bundleName);
    }

    /**
     * 加载BitmapFont(需要自行加载)
     * @param url 
     */
    public getBitmapFont(url: string, bundleName?: BUNDLENAME | string): Promise<cc.BitmapFont> {
        return this.getAsset<cc.BitmapFont>(url, cc.BitmapFont, bundleName);
    }

    /**
     * 加载龙骨数据(需要自行加载)
     * @param url 
     */
    public getSkeletonData(url: string, bundleName?: BUNDLENAME | string): Promise<sp.SkeletonData> {
        return this.getAsset<sp.SkeletonData>(url, sp.SkeletonData, bundleName);
    }

    /**
     * 文件夹数据
     */
    public getDirData<T extends cc.Asset>(dir: string, type:typeof cc.Asset, bundleName?: BUNDLENAME | string): Promise<T[]> {
        return new Promise<T[]>((resolve, reject) => {
            this.loadDir(dir, type, bundleName, (err, asset:T[])=>{
                resolve(asset);
            });
        })
    }

    /**
     * 获取分包
     * @param name 包名
     */
    public getBundle(name: string, isForce: boolean = false): Promise<cc.AssetManager.Bundle> {
        return new Promise<cc.AssetManager.Bundle>((resolve, reject) => {
            let bundle = cc.assetManager.getBundle(name);
            if (bundle) {
                resolve(bundle);
            } else if (isForce) {
                this.loadBundle(name, (err, bundle) => {
                    resolve(bundle);
                });
            } else {
                resolve(bundle);
            }
        })
    }

    public loadBundle(bundleName: string, onComplete: (err: Error, bundle: cc.AssetManager.Bundle) => void): void {
        let bundle = cc.assetManager.getBundle(bundleName);
        if (bundle) {
            onComplete(null, bundle);
            return;
        }
        cc.assetManager.loadBundle(bundleName, (err: Error, bundle: cc.AssetManager.Bundle) => {
            if (err) {
                this.loadBundle(bundleName, onComplete)
            } else {
                onComplete(err, bundle)
            }
        });
    }

    public loadBundleInnerRes(bundle: cc.AssetManager.Bundle, url: string, type: typeof cc.Asset, onComplete: (error: Error, assets: any) => void): void {
        bundle.load(url, type, (error: Error, asset: any) => {
            onComplete(error, asset)
        })
    }

    public loadResByBundleName<T extends cc.Asset>(bundleName: string, url: string, type: typeof cc.Asset, onComplete: (error: Error, assets: T) => void): void {
        let bundle: cc.AssetManager.Bundle = cc.assetManager.getBundle(bundleName);
        if (!bundle) {
            this.loadBundle(bundleName, (error, bundle) => {
                if (error) {
                    this.loadResByBundleName(bundleName, url, type, onComplete);
                } else {
                    this.loadBundleInnerRes(bundle, url, type, onComplete);
                }
            })
            return
        }
        this.loadBundleInnerRes(bundle, url, type, onComplete);
    }

    /**
    * 加载资源
    * @param url 路径
    * @param type 类型
    * @param cache 是否加入缓存
    * @param completeCallback 回调函数
    */
    public loadRes(res: string | RES_INTER, type: typeof cc.Asset, cache: boolean = false, completeCallback?: (error: Error, resource: any) => void, onProgress?: (dir: string, finish: number, total: number, item: cc.AssetManager.RequestItem) => void): void {
        let url: string;
        let bundleName: string = null;
        if (typeof (res) == 'string') {
            url = res;
        } else {
            url = res.url;
            if (res.isDir) {
                this.loadDir(url, type, res.bundleName, (error: Error, assets: cc.Asset[]) => {
                    completeCallback && completeCallback(error, assets);
                }, onProgress);
                return;
            }
            bundleName = res.bundleName;
        }

        if (bundleName && bundleName != "") {
            this.loadResByBundleName(bundleName, url, type, (error, asset: any) => {
                this.bundles[url] = res as RES_INTER;
                completeCallback && completeCallback(error, asset);
            });
        } else {
            cc.resources.load(url, type, (error, asset: any) => {
                completeCallback && completeCallback(error, asset);
            });
        }
    }

    /**
     * 加载资源(目录)
     * @param dir 目录
     * @param type 类型
     * @param bundleName 是否加入缓存
     * @param onComplete 资源回调
     * @param onProgress 进度回调
     */
    public loadDir(dir: string, type: typeof cc.Asset, bundleName?: string, onComplete?: (error: Error, assets: cc.Asset[]) => void, onProgress?: (dir: string, finish: number, total: number, item: cc.AssetManager.RequestItem) => void): void {
        let load = (bundle: cc.AssetManager.Bundle) => {
            bundle.loadDir(dir, type, (finish, total, item) => {
                onProgress && onProgress(dir, finish, total, item);
            }, (err, assets) => {
                if (err) console.error('ResMgr loadDir', err);
                onComplete && onComplete(err, assets);
            });
        }

        if (bundleName) {
            this.loadBundle(bundleName, (err, bundle) => {
                load(bundle);
            });
        } else {
            load(cc.resources);
        }
    }

    public loadRemote<T extends cc.Asset>(url: string, onComplete: (err: Error, asset: T) => void) {
        if (url == null || url == '') {
            // console.warn("loadRemote url == null");
            onComplete(null, null);
            return;
        }
        cc.assetManager.loadRemote<T>(url, (err, res: T) => {
            if (err) console.error("loadRemote url :", url);
            onComplete(err, res);
        });
    }

    public loadRemoteWithSuffix<T extends cc.Asset>(url: string, options: Record<string, any>, onComplete: (err: Error, asset: T) => void) {
        if (url == null || url == '') {
            // console.warn("loadRemote url == null");
            onComplete(null, null);
            return;
        }
        cc.assetManager.loadRemote<T>(url, options, (err, res: T) => {
            if (err) console.error("loadRemote url :", url);
            onComplete(err, res);
        });
    }


    /**
     * 预加载场景需要的资源
     * @param array 资源集合
     * @param cache 是否加入缓存
     * @param eachCall 是否每一个资源加载成功都回调
     * @param completeCallback 回调函数
     */
    public loadResArray(array: RES_INTER[], cache: boolean = false, eachCall: boolean = false, completeCallback?: (error: Error, resources: any | any[]) => void, onProgress?: (dir: string, finish: number, total: number, item: cc.AssetManager.RequestItem) => void): void {
        let index: number = 0;
        let threads: number = 0;
        let completed: boolean = false;
        let surplus: number = array.length;
        const maxThreads: number = 5;
        const length: number = array.length;
        const list: object = {};
        const loadTimes = {};
        const resources: typeof cc.Asset[] = [];
        const load: Function = () => {
            if (completed) {
                return;
            }
            while (index < length && threads <= maxThreads) {
                const data: RES_INTER = array[index];
                const url: string = data.url;
                const type: any = data.type;
                ++index;
                completed = (index == length);
                let key = `${url}:${data.bundleName || BUNDLENAME.RESOURCES}`;
                if (list[key]) {
                    console.warn('重复加载资源[' + index + ']：' + url, key);
                    completeCallback(null, null);
                } else {
                    this.loadRes(data, type, data.release !== undefined ? data.release : cache, (error, asset) => {
                        if (error) {
                            console.error('加载资源[' + url + ']失败，重新加载...', data, error);
                            this.loadRes(data, type, cache, completeCallback, onProgress);
                        } else {
                            --surplus
                            --threads;
                            load();
                            if (eachCall) {
                                completeCallback && completeCallback(error, asset);
                            } else {
                                resources.push(asset);
                                if (surplus == 0) {
                                    completeCallback && completeCallback(null, resources);
                                }
                            }
                        }
                    }, onProgress);
                    ++threads;
                    list[key] = true;
                }
            }
        };
        load();
    }

    /**
     * 预加载
     * @param array 资源集合
     * @param cache 是否加入缓存
     * @param completeCallback 回调函数
     */
    public preload(array: RES_INTER[], cache: boolean = false, completeCallback?: (error: Error, resource: any) => void, onProgress?: (dir: string, finish: number, total: number, item: cc.AssetManager.RequestItem) => void): void {
        this.loadResArray(array, cache, true, completeCallback, onProgress);
    }


    loadBundlePre(resList: RES_INTER[], complete?: Function) {
        let bundleMap = {};
        let bundleNum = 0;
        for (let i = 0; i < resList.length; i++) {
            const resInfo = resList[i];
            if (resInfo.bundleName != null) {
                if (!bundleMap[resInfo.bundleName]) {
                    bundleMap[resInfo.bundleName] = true;
                    bundleNum++;
                }
            }
        }
        if (bundleNum == 0) {
            complete && complete();
            return;
        }
        for (const bundleName in bundleMap) {
            this.loadBundle(bundleName, () => {
                bundleNum--;
                if (bundleNum <= 0) {
                    console.log('加载预设bundle完成');
                    complete && complete();
                }
            })
        }
    }

    public releaseAsset(asset: cc.Asset) {
        if (asset) {
            cc.assetManager.releaseAsset(asset);
        }
    }
}