import Singleton from "../base/Singleton";
import { STORAGE } from "../config/Enums";
import Utils from "../utils/Utils";

export default class AudioMgr extends Singleton {

    private _musicVolume: number = 1;
    private _soundVolume: number = 1;
    private _vibrate: number = 1;
    private _locate: number = 1;

    private music: number = -1;
    private sounds: object = {};

    /**
     * 音乐音量
     */
    set musicVolume(volume: number) {
        this._musicVolume = volume;
        if (-1 != this.music) {
            if (volume == 0) {
                cc.audioEngine.pauseMusic();
            } else {
                cc.audioEngine.resumeMusic()
            }
            cc.audioEngine.setMusicVolume(volume);
        }
        this.updateData();
    }

    get musicVolume(): number {
        return this._musicVolume;
    }

    /**
     * 音效音量
     */
    set soundVolume(volume: number) {
        this._soundVolume = volume;
        for (const key in this.sounds) {
            const id: number = this.sounds[key];
            if (cc.audioEngine.getState(id) != cc.audioEngine.AudioState.ERROR) {
                cc.audioEngine.setVolume(id, volume);
            }
        }
        cc.audioEngine.setEffectsVolume(volume);
        this.updateData();
    }

    get soundVolume(): number {
        return this._soundVolume;
    }

    /**
     * 振动
     */
    set vibrate(vibrate: number) {
        this._vibrate = vibrate;
        this.updateData();
    }

    get vibrate(): number {
        return this._vibrate;
    }

    /**
     * 定位
     */
    set locate(locate: number) {
        this._locate = locate;
        this.updateData();
    }

    get locate(): number {
        return this._locate;
    }

    updateData() {
        Utils.setItem(STORAGE.AUDIO_DATA, {
            music: this._musicVolume,
            sound: this._soundVolume,
            vibrate: this._vibrate,
            locate: this._locate,
        });
        // app.event.emit(Events.SETTING_CHANGE)
    }

    constructor() {
        super();
        let data = Utils.getItem(STORAGE.AUDIO_DATA, {});
        this.musicVolume = this.getInitData(data.music);
        this.soundVolume = this.getInitData(data.sound);
        this.vibrate = this.getInitData(data.vibrate);
        this.locate = this.getInitData(data.locate);
        // cc.audioEngine.setMaxAudioInstance(20);
    }

    getInitData(value: number) {
        if (value == null) {
            return 1;
        }
        return value;
    }

    /**
     * 播放音乐
     * @param url 地址
     * @param loop 是否循环
     */
    private musicAssets = {};
    public async playMusic(url: string, bundle?: string, loop: boolean = true): Promise<number> {
        this.music != -1 && cc.audioEngine.stop(this.music);
        const clip: cc.AudioClip = await app.resMgr.getAudioClip(url, bundle);
        if (clip) {
            this.music = cc.audioEngine.playMusic(clip, loop);
            // app.resMgr.addRef(clip);
            cc.audioEngine.setMusicVolume(this._musicVolume)
            this.musicAssets[this.music] = clip;
        }
        return this.music;
    }

    musicList: { name: string, bundle?: string }[] = [];
    private curIdx: number = 0;
    private timerId: number = null;
    public playMusicList(urls: { name: string, bundle?: string }[]): void {
        this.musicList = urls;
        this.curIdx = 0;
        if (this.musicList == null || this.musicList.length <= 0) {
            return;
        }
        if (this.timerId != null) {
            clearTimeout(this.timerId);
            this.timerId = null;
        }
        let play = async () => {
            let data = this.musicList[this.curIdx];
            let id = this.playMusic(data.name, data.bundle);
            let duration = cc.audioEngine.getDuration(await id);
            this.timerId = setTimeout(() => {
                this.curIdx = (this.curIdx + 1) % this.musicList.length;
                play();
            }, Math.floor(duration) * 1000);
        }
        play();
    }

    /**
     * 停止播放音乐
     */
    public stopMusic(): void {
        if (this.timerId != null) {
            clearTimeout(this.timerId);
            this.timerId = null;
            // this.musicList = null;
        }

        if (this.music != -1) {
            // app.resMgr.decRef(this.musicAssets[this.music]);
            delete this.musicAssets[this.music];
            if (cc.audioEngine.getState(this.music) != cc.audioEngine.AudioState.ERROR) {
                cc.audioEngine.stopMusic();
                this.music = -1;
            }
        }
    }

    /**
     * 播放音效
     * @param url 地址
     * @param loop 是否循环
     */
    public async playSound(url: string, loop: boolean = false, callback?: Function, bundle?: string): Promise<number> {
        if (this._soundVolume == 0) {
            return -1;
        }
        const clip: cc.AudioClip = await app.resMgr.getAudioClip(url, bundle);
        if (clip) {
            return this.play(clip, loop, (id: number) => {
                delete this.sounds[id];
                callback && callback();
            });
        }
        return -1;
    }

    public play(clip: cc.AudioClip, loop: boolean = false, callback?: Function) {
        const id: number = cc.audioEngine.play(clip, loop, this._soundVolume);
        this.sounds[id] = 1;
        cc.audioEngine.setFinishCallback(id, () => {
            callback && callback(id);
        });
        return id;
    }

    public audioMap: any = {};
    public singleName: string = null;
    /**
     * @param name 播放音效名称
     * @param clip 音频
     * @param times 次数 -1为循环
     * @param callback 回调
     * @param isSingle 是否单个播放
     * @param isFirst 是否首次 用于第一次播放时做存值判断(不要传值)
     * @returns 
     */
    public playWithTimes(name: string, clip: cc.AudioClip, times: number = 1, callback?: Function, isSingle: boolean = false, isFirst: boolean = true) {
        if (!clip) return;
        if (times == 0) {
            this.stopAudioTimes(name);
            return;
        }
        if (isSingle && this.singleName != name) {
            if (this.singleName != null) {
                this.stopAudioTimes(this.singleName);
            }
            this.singleName = name;
        }
        let loop = false;
        if (times == -1) {
            loop = true;
        }
        const id: number = cc.audioEngine.play(clip, loop, this._soundVolume);
        if (this.audioMap[name] != null || isFirst) {
            this.audioMap[name] = { id: id, times: times, clip: clip, callback: callback, isSingle: isSingle };
        }
        this.audioMap[name].times = times;
        if (loop || this.audioMap[name].isPause) return;
        cc.audioEngine.setFinishCallback(id, () => {
            if (!this.audioMap[name] || times - 1 == 0) {
                this.stopAudioTimes(name);
                return;
            }
            if (this.audioMap[name].isPause) {
                this.audioMap[name].id = -1;
                return;
            }
            let nextId = this.playWithTimes(name, clip, times - 1, callback, false, false);
            callback && callback(times - 1, nextId);
        });
        return id;
    }

    public stopAudioTimes(name: string) {
        if (this.audioMap[name]) {
            this.stop(this.audioMap[name].id);
            this.audioMap[name] = null;
        }
    }

    public pauseAudioTimes(name: string) {
        if (this.audioMap[name]) {
            this.pause(this.audioMap[name].id);
            this.audioMap[name].isPause = true;
        }
    }

    public resumeAudioTimes(name: string) {
        if (this.audioMap[name]) {
            this.audioMap[name].isPause = false;
            let data = this.audioMap[name];
            if (data.id && data.id > 0) {
                this.resume(data.id);
            } else {
                this.playWithTimes(name, data.clip, (data.times || 1) - 1, data.callback, data.isSingle, false);
            }
        }
    }

    public stop(id: number) {
        cc.audioEngine.stop(id);
    }

    public pause(id: number) {
        cc.audioEngine.pause(id);
    }

    public resume(id: number) {
        cc.audioEngine.resume(id);
    }

    /**
     * 停止音效
     * @param id 
     */
    public stopSound(id: number): void {
        if (-1 != id) {
            if (cc.audioEngine.getState(id) != cc.audioEngine.AudioState.ERROR) {
                cc.audioEngine.stop(id);
                delete this.sounds[id];
            }
        }
    }

    /**
     * 停止所有音效
     */
    public stopSounds(): void {
        for (const key in this.sounds) {
            const id: number = Number(key);
            if (cc.audioEngine.getState(id) != cc.audioEngine.AudioState.ERROR) {
                cc.audioEngine.stop(id);
            }
        }
        this.sounds = {};
    }
}