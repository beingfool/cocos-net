import Singleton from "../base/Singleton";
import { SEARCH_TYPE } from "../config/Enums";
import { DB_INFO_INTER } from "../config/Interface";

export default class GameCtrl extends Singleton {

    private typeList: { name: string, list: string[] }[] = [];

    login() {
    }

    getServerTime() {
        return Date.now();
    }

    getHistory() {
        return app.data.searchHistory || [];
    }

    search(str: string, type: SEARCH_TYPE) {
        console.log('GameCtrl >>>>>>>> 21', str, type);
        if (type != SEARCH_TYPE.TYPE) {
            let list = app.data.searchHistory || [];
            for (let i = 0; i < list.length; i++) {
                if (list[i].search == str && list[i].type == type) {
                    list.splice(i, 1);
                    break;
                }
            }
            list.splice(0, 0, { search: str, type: type });
            app.data.searchHistory = list;
        }
        app.event.emit(app.eventName.SEARCH_INFO, { search: str, type: type });
    }

    getSearchList(str: string, searchType: SEARCH_TYPE) {
        let dbMap = app.data.dbJson || {};
        let list: DB_INFO_INTER[] = [];
        if (searchType == SEARCH_TYPE.TYPE) {
            let typeList = this.getTypeMap();
            for (let i = 0; i < typeList.length; i++) {
                let names = typeList[i].list || [];
                let typeName = typeList[i].name;
                for (let j = 0; j < names.length; j++) {
                    if(str == typeName){
                        list = list.concat(dbMap[typeName][names[j]]);
                    }else if(names[j] == str){
                        list = dbMap[typeName][str]
                    }
                }
            }
        } else {
            for (const type in dbMap) {
                const map = dbMap[type];
                for (const childType in map) {
                    let list = map[childType] || [];
                    for (let i = 0; i < list.length; i++) {
                        const info = list[i];
                        let typeName = info.name || ``;
                        if (searchType == SEARCH_TYPE.MAIN && typeName.indexOf(str) != -1) {
                            list.push(info);
                        } else if (searchType == SEARCH_TYPE.SIGN) {
                            let lls = info.sign || [];
                            for (let j = 0; j < lls.length; j++) {
                                if (lls[j].indexOf(str) != -1) {
                                    list.push(info);
                                    break;
                                }
                            }
                        }
                    }
                }
            }
        }
        return list;
    }

    getTypeMap() {
        if(this.typeList == null || this.typeList.length == 0){
            this.typeList = [];
            let list = Object.keys(app.data.dbJson);
            for (let i = 0; i < list.length; i++) {
                const name = list[i];
                this.typeList.push({
                    name: name,
                    list: Object.keys(app.data.dbJson[name]),
                })
            }
        }
        return this.typeList;
    }

    //=================== record ====================
    getRecordInfo(key: string, def?: any) {
        let data = app.data.recordDatas || {};
        if (data[key] != null) {
            return data[key];
        }
        return def;
    }

    setRecordInfo(key: string, info: any) {
        let data = app.data.recordDatas || {};
        data[key] = info;
        app.data.recordDatas = data;
    }
} 