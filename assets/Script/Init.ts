import Singleton from "./base/Singleton";
import Events from "./config/Events";
import { Main } from "./config/MainConfig";
import MainConst from "./config/MainConst";
import GameCtrl from "./ctrl/GameCtrl";
import GameData from "./data/GameData";
import AudioMgr from "./mgr/AudioMgr";
import NodeMgr from "./mgr/NodeMgr";
import PoolMgr from "./mgr/PoolMgr";
import RemindMgr from "./mgr/RemindMgr";
import ResMgr from "./mgr/ResMgr";
import SpriteMgr from "./mgr/SpriteMgr";
import TweenMgr from "./mgr/TweenMgr";
import ViewMgr from "./mgr/ViewMgr";
import ArrayUtil from "./utils/ArrayUtils";
import DateUtil from "./utils/DateUtil";
import MathUtil from "./utils/MathUtil";
import StringUtil from "./utils/StringUtil";
import Timer from "./utils/Timer";
import Utils from "./utils/Utils";



// 命名空间声明，用于代码提示
declare global {
    namespace app {
        let data: GameData;
        let ctrl: GameCtrl;
        let event: cc.EventTarget;
        let resMgr: ResMgr;
        let nodeMgr: NodeMgr;
        let viewMgr: ViewMgr;
        let poolMgr: PoolMgr;
        let tweenMgr: TweenMgr;
        let audioMgr: AudioMgr;
        let spriteMgr: SpriteMgr;
        let remindMgr: RemindMgr;

        let arrayUtil: typeof ArrayUtil;
        let dateUtil: typeof DateUtil;
        let mathUtil: typeof MathUtil;
        let stringUtil: typeof StringUtil;
        let utils: typeof Utils;

        let timer: Timer;

        let cons: MainConst;

        let main: typeof Main;
        let eventName: typeof Events;
    }
}


export default class Init extends Singleton {

    constructor() {
        super();

        // 命名空间实际定义
        window['app'] = window.app || {
            data: null,
            ctrl: null,
            event: null,
            resMgr: null,
            nodeMgr: null,
            viewMgr: null,
            poolMgr: null,
            tweenMgr: null,
            audioMgr: null,
            spriteMgr: null,
            remindMgr: null,

            arrayUtil: null,
            dateUtil: null,
            mathUtil: null,
            stringUtil: null,
            utils: null,

            timer: null,

            cons:null,

            main: null,
            eventName: null,
        };

        app.data = GameData.getInstance();
        app.ctrl = GameCtrl.getInstance();
        app.event = new cc.EventTarget();
        app.resMgr = ResMgr.getInstance();
        app.nodeMgr = NodeMgr.getInstance();
        app.viewMgr = ViewMgr.getInstance();
        app.poolMgr = PoolMgr.getInstance();
        app.tweenMgr = TweenMgr.getInstance();
        app.audioMgr = AudioMgr.getInstance();
        app.spriteMgr = SpriteMgr.getInstance();
        app.remindMgr = RemindMgr.getInstance();

        app.arrayUtil = ArrayUtil;
        app.dateUtil = DateUtil;
        app.mathUtil = MathUtil;
        app.stringUtil = StringUtil;
        app.utils = Utils;
        
        app.timer = Timer.getInstance();

        app.cons = new MainConst();

        app.main = Main;
        app.eventName = Events;
    }
}


if (CC_EDITOR) {
    // 重写update方法 达到在编辑模式下 自动播放动画的功能
    sp.Skeleton.prototype['update'] = function (dt) {
        if (CC_EDITOR) {
            cc['engine']._animatingInEditMode = 1;
            cc['engine'].animatingInEditMode = 1;
        }
        if (this.paused) return;

        dt *= this.timeScale * sp['timeScale'];

        if (this.isAnimationCached()) {

            // Cache mode and has animation queue.
            if (this._isAniComplete) {
                if (this._animationQueue.length === 0 && !this._headAniInfo) {
                    let frameCache = this._frameCache;
                    if (frameCache && frameCache.isInvalid()) {
                        frameCache.updateToFrame();
                        let frames = frameCache.frames;
                        this._curFrame = frames[frames.length - 1];
                    }
                    return;
                }
                if (!this._headAniInfo) {
                    this._headAniInfo = this._animationQueue.shift();
                }
                this._accTime += dt;
                if (this._accTime > this._headAniInfo.delay) {
                    let aniInfo = this._headAniInfo;
                    this._headAniInfo = null;
                    this.setAnimation(0, aniInfo.animationName, aniInfo.loop);
                }
                return;
            }

            this._updateCache(dt);
        } else {
            this._updateRealtime(dt);
        }
    }
}