import { CLICK_ARG_INTER } from "../config/Interface";

const { ccclass, property } = cc._decorator;
@ccclass
export default class BaseComponent extends cc.Component {
    
    protected arg: any;
    protected isOpen: boolean;
    protected listeners: { event: string, func: Function, target: cc.Node }[] = [];

    
    // ========================= 周期函数 =========================
    onLoad() {
        this.init();
        this.addEventListener();
    }

    start(): void {
    }

    onEnable(): void {
        this.onShow();
    }

    onDisable() {
        this.onHide();
    }

    onDestroy() {
        this.removeAllListener();
    }


    // ========================= 自定义周期函数 =========================

    init() { }

    addEventListener() { }

    onShow() { }

    onHide() { }

    open() {
        this.isOpen = true;
    }

    close() {
        this.isOpen = false;
    }

    public eventEmit(event: string, arg1?: any, arg2?: any, arg3?: any, arg4?: any, arg5?: any) {
        app.event.emit(event, arg1, arg2, arg3, arg4, arg5);
    }

    public eventOn(event: string, func: Function, target: any) {
        for (let i = 0; i < this.listeners.length; i++) {
            if (this.listeners[i].event == event) {
                console.error('BaseComponent eventOn', event);
                return;
            }
        }
        this.listeners.push({
            event: event,
            func: func,
            target: target,
        });
        app.event.on(event, func, target);
    }


    public removeAllListener() {
        for (let i = 0; i < this.listeners.length; i++) {
            const listener = this.listeners[i];
            app.event.off(listener.event, listener.func, listener.target);
        }
        this.listeners = [];
    }

    /**
     * @param parent 父节点 
     * @param name 节点名
     */
    public seekChild(parent: cc.Node, name: string): cc.Node {
        if (!parent) {
            return;
        }

        if (parent.name === name) {
            return parent
        }

        let children = parent.children;
        for (let i = 0; i < children.length; ++i) {
            let node = this.seekChild(children[i], name)
            if (node) {
                return node
            }
        }
    }


    public forEachChild(parent: cc.Node, callback: (child: cc.Node) => void) {
        if (!parent) {
            return;
        }
        let children = parent.children;
        for (let i = 0; i < children.length; ++i) {
            callback && callback(children[i]);
            this.forEachChild(children[i], callback);
        }
    }


    /**
     * 按钮点击事件
     * @param button 按钮
     * @param callback 回调函数(tagert: cc.Button)
     * @param target this对象
     */
    public onClick(button: cc.Node | cc.Button, callback: Function, target?: any, data?: CLICK_ARG_INTER): void {
        app.nodeMgr.onClick(button, callback, target, data);
    }

    /**
     * 注销按钮点击事件
     * @param button 按钮
     * @param callback 回调函数(tagert: cc.Button)
     * @param target this对象
     */
    public offClick(button: cc.Node | cc.Button, callback?: Function, target?: any): void {
        app.nodeMgr.offClick(button, callback, target)
    }

}