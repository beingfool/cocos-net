import BaseComponent from "./BaseComponent";

const { ccclass, property } = cc._decorator;
@ccclass
export default class BaseUI extends BaseComponent {

    protected arg:any = {};
    
    public setArg(arg:any){
        this.arg = arg;
        this.updateUI();
    }

    updateUI(){

    }

}