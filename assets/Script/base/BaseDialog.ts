import BaseUI from "./BaseUI";

const { ccclass, property } = cc._decorator;
@ccclass
export default class BaseDialog extends BaseUI {

    protected arg:any;
    
    protected isDelayShow: boolean = true;
    protected showCustom: boolean = false;
    protected openCallback: Function = null;
    protected nodeAct: cc.Tween = null;
    protected rootAct: cc.Tween = null;

    onAdaptUI(): void {
        app.nodeMgr.adaptNode2MinScreen(this.root);
    }

    open () {
        this.node.opacity = 0;
        this.root.opacity = 0;
        this.root.scale = 0;
        let scale = app.nodeMgr.getAdaptMinScale(this.root);
        this.nodeAct && this.nodeAct.stop();
        this.rootAct && this.rootAct.stop();
        this.nodeAct = cc.tween(this.node).to(0.2, {opacity:255}).start();
        this.rootAct = cc.tween(this.root).to(0.2, {scale:scale, opacity:255}, cc.easeCubicActionOut()).call(()=>{
            this.openCallback && this.openCallback(); 
        }).start();
        this.doCloseBtnAct();
    }

    doCloseBtnAct () {
        // if(this.data.showClose != false){
        //     this.btnClose.node.active = false;
        //     SDKMgr.delayShowCloseBtn(()=>{
        //         this.btnClose.node.active = true;
        //     });
        // }
    }

    doCloseAct (callBack?:Function) {
        this.nodeAct && this.nodeAct.stop();
        this.rootAct && this.rootAct.stop();
        this.nodeAct = cc.tween(this.node).to(0.15, {opacity:0}).start();
        this.rootAct = cc.tween(this.root).to(0.2, {scale:0.5}).call(()=>{
            callBack && callBack()
            this.node.opacity = 255;
            this.root.opacity = 255;
            this.root.scale = app.nodeMgr.getAdaptMinScale(this.root);;
        }).start();
    }

    public setArg(arg:any){
        this.arg = arg;
        this.updateUI();
    }

    updateUI(){

    }

}