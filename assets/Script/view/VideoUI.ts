import { CCMVideo, EventType } from "../../../coco-mat/lib/CCMVideo/CCMVideo";
import BaseUI from "../base/BaseUI";
import ComColumn from "../com/ComColumn";
import { VIDEO_CB_TYPE } from "../config/Enums";
import { DB_INFO_INTER } from "../config/Interface";
import { UI } from "../config/MainConfig";
const { ccclass, property } = cc._decorator;

@ccclass
export default class VideoUI extends BaseUI {

    @property(cc.Node)
    btnBack: cc.Node = null;
    @property(cc.Label)
    labTop: cc.Label = null;

    @property(CCMVideo)
    video: CCMVideo = null;

    @property(ComColumn)
    column: ComColumn = null;

    // @property(cc.Node)
    // nodeUI:cc.Node = null;
    // @property(cc.Node)
    // btnLeft:cc.Node = null;
    // @property(cc.Node)
    // btnPause:cc.Node = null;
    // @property(cc.Node)
    // btnStop:cc.Node = null;
    // @property(cc.Node)
    // btnRight:cc.Node = null;

    // @property(cc.ProgressBar)
    // progVoice:cc.ProgressBar = null;
    // @property(cc.Slider)
    // sliderVoice:cc.Slider = null;
    // @property(cc.Node)
    // btnSpeed:cc.Node = null;
    // @property(cc.Node)
    // nodeSpeed:cc.Node = null;
    // @property(cc.Node)
    // btnRate:cc.Node = null;
    // @property(cc.Node)
    // btnFull:cc.Node = null;

    @property(cc.Label)
    labName: cc.Label = null;
    @property(cc.Label)
    labDesc: cc.Label = null;

    @property(cc.Node)
    episode: cc.Node = null;
    @property(cc.Node)
    btnEpisode: cc.Node = null;

    @property(cc.Node)
    recommend: cc.Node = null;

    @property(cc.Prefab)
    searchItem: cc.Prefab = null;

    private videoIndex: number = 0;
    private curData: DB_INFO_INTER;
    private videoUrls: string[] = [];
    private totalTime: number = 0;

    // private speedList:cc.Node[] = [];

    init(): void {
        this.onClick(this.btnBack, this.onClickBack, this);

        // this.onClick(this.btnLeft, this.onClickLeft, this);
        // this.onClick(this.btnPause, this.onClickPause, this);
        // this.onClick(this.btnStop, this.onClickStop, this);
        // this.onClick(this.btnRight, this.onClickRight, this);

        // this.onClick(this.btnSpeed, this.onClickSpeed, this);
        // this.onClick(this.btnFull, this.onClickFull, this);
    }

    addEventListener(): void {
        this.eventOn(app.eventName.SELE_INFO, this.updateView, this);
    }

    open(): void {
        super.open();
        this.updateView(this.arg);
    }

    initView() {
        this.column.setData(this.onVideoCallback.bind(this));
        // 
    }

    updateView(data: DB_INFO_INTER) {
        this.curData = data;
        this.videoUrls = data.urls || [];
        this.updateVideo(0);
    }

    updateVideo(idx: number) {
        this.videoIndex = idx;
        this.playVideo(idx, false);
    }

    onClickBack() {
        app.viewMgr.hideUI(UI.VIDEO)
    }

    private onVideoEvent(sender, event) {
        switch (event) {
            case EventType.READY:
                // console.log('CCMVideoExample', this.video.width + ', ' + this.video.height);
                // this.updateDuration();
                // this.updateCurrentTime(true);
                this.updateCurrentTime();
                break;
            case EventType.PLAYING:
            case EventType.PAUSED:
            case EventType.STOPPED:
                this.column.updateState(event);
                break;
            case EventType.COMPLETED:
                // this.next();
                break;
        }
    }

    playVideo(idx: number, isPlay: boolean = true) {
        this.video.clip = this.videoUrls[idx];
        if (isPlay) {
            this.video.play();
        }
        this.totalTime = this.video.duration;
    }

    onVideoCallback(status: VIDEO_CB_TYPE, data?: any) {
        switch (status) {
            case VIDEO_CB_TYPE.PREVIOUS:
                this.videoIndex--;
                let len1 = this.videoUrls.length;
                if (this.videoIndex < 0) this.videoIndex += len1;
                this.playVideo(this.videoIndex % len1);
                break;
            case VIDEO_CB_TYPE.NEXT:
                this.videoIndex++;
                this.playVideo(this.videoIndex % this.videoUrls.length);
                break;
            case VIDEO_CB_TYPE.PLAY:
                this.video.play();
                // this.video.resume();
                break;
            case VIDEO_CB_TYPE.PAUSE:
                this.video.pause();
                break;
            case VIDEO_CB_TYPE.STOP:
                this.video.stop();
                break;
            case VIDEO_CB_TYPE.SPEED:
                this.video["_impl"]._video.playbackRate = data || 1;
                break;
            case VIDEO_CB_TYPE.TIME:
                this.video.currentTime = Math.floor(this.totalTime * data);
                break;
            case VIDEO_CB_TYPE.FULL:
                // this.video.
                break;
        }
    }

    updateCurrentTime(){
        let progress = this.video.currentTime / this.totalTime;
        this.column.updateCurrentTime(progress, this.totalTime);
    }

    protected update(dt: number) {
        this.updateCurrentTime();
    }

    // onClickLeft(){
    //     if(this.curIdx > 0){
    //         this.updateVideo(this.curIdx - 1);
    //     }
    // }

    // onClickPause(){
    //     if(this.videoPlayer){
    //         this.videoPlayer.pause();
    //     }
    // }

    // onClickStop(){
    //     if(this.videoPlayer){
    //         this.videoPlayer.stop();
    //     }
    // }

    // onClickRight(){
    //     let list = this.curData.urls || [];
    //     if(this.curIdx < list.length - 1){
    //         this.updateVideo(this.curIdx + 1);
    //     }
    // }

    // onClickSpeed(){
    //     this.nodeSpeed.active = !this.nodeSpeed.active;
    // }

    // onClickFull(){
    //     if(this.videoPlayer){
    //         this.videoPlayer.isFullscreen = true;
    //     }
    // }

    // private clickTime = 0;
    // videoPlayerEvent(video:cc.VideoPlayer, type){
    //     switch (type) {
    //         case cc.VideoPlayer.EventType.META_LOADED:
    //             video.play();
    //             break;
    //         case cc.VideoPlayer.EventType.CLICKED:
    //             let time = Date.now();
    //             if(time < this.clickTime + 500){
    //                 video.isFullscreen = !video.isFullscreen;
    //             }
    //             this.clickTime = time;
    //             if(!video.isFullscreen){
    //                 if(video.isPlaying()){
    //                     video.pause();
    //                 }else{
    //                     video.resume();
    //                 }
    //             }
    //             break;
    //         default:
    //             break;
    //     }
    // }

}
