import BaseUI from "../base/BaseUI";
const { ccclass, property } = cc._decorator;

@ccclass
export default class WaitUI extends BaseUI {

    @property(cc.Node)
    sprLoad:cc.Node = null;

    onLoad(): void {
        super.onLoad();
        this.onClick(this.node, this.onClickWait, this);
    }

    open(): void {
        super.open();
    }

    onEnable(): void {
        this.scheduleOnce(this.showLoad, 3);
    }

    onDisable(): void {
        this.unschedule(this.showLoad);
        this.unschedule(this.hideLoad);
        this.sprLoad.active = false;
    }

    showLoad(){
        if(this.sprLoad && cc.isValid(this.sprLoad)){
            this.sprLoad.active = true;        
        }
        this.scheduleOnce(this.hideLoad, 10);
    }

    hideLoad(){
        app.viewMgr.clearWait();
        // if(this.sprLoad && cc.isValid(this.sprLoad)){
        //     this.sprLoad.active = false;        
        // }
    }

    onClickWait(){
        app.viewMgr.logWaitInfo();
    }

}
