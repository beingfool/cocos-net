import BaseUI from "../base/BaseUI";

const { ccclass, property } = cc._decorator;

@ccclass
export default class LoadingUI extends BaseUI {
    
    @property(cc.ProgressBar)
    loadingBar: cc.ProgressBar = null;

    private progress:number = 0;

    init() {
        this.loadingBar.progress = 0;
        this.schedule(this.updateTime, 0.001);
    }

    addEventListener() {
        this.eventOn(app.eventName.LOAD_PROGERSS, this.onLoadRes, this);
        
    }

    lastProg:number;
    onLoadRes(progress: number) {
        if(this.progress != this.loadingBar.progress){
            this.lastProg = this.progress;
        }
        this.progress = progress;
    }

    updateTime(dt: number): void {
        if(this.loadingBar.progress < this.progress){
            let num = 0.02;
            if(this.loadingBar.progress < this.lastProg){
                num = 0.04;
            }
            this.loadingBar.progress += num;
        }
    }
    
}