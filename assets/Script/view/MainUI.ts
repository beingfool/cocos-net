import BaseUI from "../base/BaseUI";
import ComSearch from "../com/ComSearch";
import List from "../com/List";
import SearchItem from "../com/SearchItem";
import TypeItem from "../com/TypeItem";
import { SEARCH_TYPE } from "../config/Enums";
import { DB_INFO_INTER } from "../config/Interface";
const { ccclass, property } = cc._decorator;

@ccclass
export default class MainUI extends BaseUI {

    @property(ComSearch)
    comSearch: ComSearch = null;

    @property(cc.Node)
    typeLayout: cc.Node = null;

    @property(List)
    scrollList: List = null;

    @property(cc.Prefab)
    typeItem: cc.Prefab = null;

    @property(cc.Prefab)
    searchItem: cc.Prefab = null;

    private typeList: cc.Node[] = [];
    private searchList: DB_INFO_INTER[] = [];


    init() {
        this.comSearch.node.active = false;
        // this.typeLayout.active = false;
    }

    addEventListener() {
        this.eventOn(app.eventName.SEARCH_INFO, this.onSearchInfo, this)
    }

    open(): void {
        super.open();
        this.initView();
    }

    initView() {
        let list = app.ctrl.getTypeMap();
        app.nodeMgr.addListItem(list.length, this.typeList, this.typeItem, this.typeLayout, (item: cc.Node, idx: number) => {
            item.getComponent(TypeItem).setData(list[idx], (visible:boolean)=>{
                this.onSeleType(idx, visible);
            });
        });
        this.onSearchInfo({search:list[0].name, type:SEARCH_TYPE.TYPE});
    }

    updateItems(list: DB_INFO_INTER[]) {
        this.searchList = list;
        this.scrollList.numItems = this.searchList.length;
    }

    onListRander(item: cc.Node, idx: number) {
        item.getComponent(SearchItem).setData(this.searchList[idx]);
    }

    onSeleType(idx:number, visible:boolean){
        if(!visible) return;
        for (let i = 0; i < this.typeList.length; i++) {
            if(idx != i){
                this.typeList[i].getComponent(TypeItem).setLayerVisible(false)
            }
        }
    }

    onSearchInfo(data: { search: string, type: SEARCH_TYPE }) {
        let list = app.ctrl.getSearchList(data.search, data.type);
        this.updateItems(list);
    }

}
