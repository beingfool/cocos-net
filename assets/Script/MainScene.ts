import BaseComponent from "./base/BaseComponent";
import { STORAGE } from "./config/Enums";
import { UI } from "./config/MainConfig";
import Init from "./Init";

const {ccclass, property} = cc._decorator;

@ccclass
export default class MainScene extends BaseComponent {

    @property(cc.Node)
    uiLayer: cc.Node = null;

    @property(cc.Node)
    dialogLayer: cc.Node = null;

    @property(cc.Node)
    tipsLayer: cc.Node = null;

    @property(cc.Node)
    loadingView: cc.Node = null;

    init(): void {
        Init.getInstance();
        app.viewMgr.initLayer(this.uiLayer, this.dialogLayer, this.tipsLayer);
        this.preload();
        this.initView();
    }
    
    /**
     * 预加载
     */
    private preload(): void {
        console.time("load");
        let progress: number = 0;
        const length: number = app.main.preload.length;
        let dirProg = {};
        let lastProg = 0;
        let updateProg = (isFinish: boolean = false) => {
            let dirTotalProg = 0;
            for (const dir in dirProg) {
                if (isFinish && dirProg[dir] >= 1) {
                    dirProg[dir] = 0;
                }
                dirTotalProg += dirProg[dir];
            }
            let prog = (progress + dirTotalProg) / length;
            if (prog - lastProg >= 0.02) {
                lastProg = prog;
                app.event.emit(app.eventName.LOAD_PROGERSS, prog);
            }
        }
        app.resMgr.loadBundlePre(app.main.preload, () => {
            app.resMgr.preload(app.main.preload, false, (error: any, res: any) => {
                if (error) {
                    this.preload();
                } else {
                    progress++;
                    updateProg(true);
                    if (progress == length) {
                        console.timeEnd("load");
                        this.initJson();
                        this.loadFinish();
                        return;
                    }
                }
            }, (dir: string, finish: number, total: number, item: cc.AssetManager.RequestItem) => {
                dirProg[dir] = dirProg[dir] || 0;
                dirProg[dir] = Math.max(dirProg[dir], finish / total);
                updateProg();
            });
        });
    }

    preloadLate() {
        app.resMgr.loadBundlePre(app.main.preloadLate, () => {
            app.resMgr.preload(app.main.preloadLate, false, (error: any, res: any) => {
                if (error) {
                    this.preloadLate();
                }
            });
        });
    }

    initView(){
        this.loadingView.active = true;
        
    }

    private clickTime = 0;
    videoPlayerEvent(video:cc.VideoPlayer, type){
        switch (type) {
            case cc.VideoPlayer.EventType.META_LOADED:
                video.play();
                break;
            case cc.VideoPlayer.EventType.CLICKED:
                let time = Date.now();
                if(time < this.clickTime + 500){
                    video.isFullscreen = !video.isFullscreen;
                }
                this.clickTime = time;
                if(!video.isFullscreen){
                    if(video.isPlaying()){
                        video.pause();
                    }else{
                        video.resume();
                    }
                }
                break;
            default:
                break;
        }
    }

    loadcfg(res: any) {
        this.initData();
    }

    /**
     * 初始化
     */
    private initData(): void {
        app.data.recordDatas = app.utils.getItem(STORAGE.RECORD_DATAS, {});
    }

    /**
     * 初始化Json
     */
    private async initJson() {
        let dbJson = await app.resMgr.getJsonAsset(app.main.json.dbJson);
        app.data.dbJson = dbJson.json;
        app.resMgr.releaseAsset(dbJson);
    }

    loadFinish() {
        this.preloadLate();
        app.ctrl.login();
        this.schedule(this.updateTimer, 1)
        this.loadingView.active = false;
        app.viewMgr.showUI(UI.MAIN);
    }

    updateTimer(dt: number) {
    }

}
