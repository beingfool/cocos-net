import { BUNDLENAME, DB_TYPE, SEARCH_TYPE, VIEW_TYPE } from "./Enums"

export interface UI_INTER {
    ui: cc.Node,
    isLoading?: boolean,
    viewType: number
    scene?: string,
}


export interface RES_INTER {
    url: string,
    release?: boolean,
    type: typeof cc.Asset,
    bundleName?: BUNDLENAME | string,
    isDir?: boolean,
}


export interface VIEW_INTER {
    url: string,
    type: typeof cc.Asset,
    viewType: VIEW_TYPE,
    isRedBag?: boolean,
    isDestroy?: boolean,
    tsName?: string,
    bundleName?: BUNDLENAME | string,
}


export interface CLICK_ARG_INTER {
    clickTime?: number,
    noSound?: boolean
    effect?: string,
    boundle?: BUNDLENAME,
}

export interface SPRITE_ADAPT_INTER {
    width?: number,
    height?: number,
    isAdaptMax?: Boolean,
    isAdaptMin?: Boolean,
    isScale?: boolean,
}

export interface ACT_CFG_INTER {
    time: number,
    delayTime?: number,
    doTimes?: number,
    value?: number,
    pos?: cc.Vec2,
}

export interface DB_INFO_INTER {
    type: DB_TYPE,
    icon: string,
    urls: string[],
    desc?: string,
    author?: string,
    name: string,
    sign: string[],
}

export interface SEARCH_HISTORY_INTER {
    search: string,
    type: SEARCH_TYPE,
}