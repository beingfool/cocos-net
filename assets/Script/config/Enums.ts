
/**
 * 保存的记录数据
 */
export enum STORAGE {
    AUDIO_DATA = `AUDIO_DATA`,
    RECORD_DATAS = `RECORD_DATAS`,

    HISTORY_SEARCH = `HISTORY_SEARCH`,
    // HISTORY_RECORD = `HISTORY_RECORD`,
};

export enum RECORD_KEY {
}

export enum BUNDLENAME {
    RESOURCES = 'resources',
};

export enum ACTION_TYPE {
    DEFAULT,
    ROTATE,
    BREATH,
    TWINKLE,
    UP,
    TREMBLE,
    ROCK,
    MOVE,
}


export enum VIEW_TYPE {
    UI = 1,
    DIALOG = 2,
    TIPS = 3,
    TOP = 4,
}

export enum POOL_NAME {
    // CARD = "card",
}

export enum REMIND_NAME {
}

export enum SEARCH_TYPE {
    TYPE = 0,
    MAIN = 1,
    SIGN = 2,
}

export enum DB_TYPE {
    VIDEO    = 1,
    ANIM     = 2,
    COMIC    = 3,
    GAME     = 4,
    MUSIC    = 5,
}

export enum VIDEO_CB_TYPE {
    PREVIOUS    = 1,
    NEXT        = 2,
    PLAY        = 3,
    PAUSE       = 4,
    STOP        = 5,
    SPEED       = 6,
    TIME        = 7,
    FULL        = 8,
}


