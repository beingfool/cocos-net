import { VIEW_TYPE } from "./Enums";
import { VIEW_INTER } from "./Interface";

const atlas = {
};

const spine = {
};

const sprite = {
};

const font = {
};

const audio = {
    // bgm: 'audio/bgm',
    // click: 'audio/click',
};

const prefab = {
    /************** main **************/
    // UI
    MainUI: 'prefab/view/MainUI',
    VideoUI: 'prefab/view/VideoUI',
    WaitUI: 'prefab/view/WaitUI',
    // Com
    Tips: 'prefab/com/Tips',
};

const json = {
    dbJson: 'config/db',
};

const dir = {
    config: "config",
    audio: "audio",
    spine: "spine",

    view: "prefab/view",
    com: "prefab/com",
}

export const Main = {
    json: json,
    atlas: atlas,
    spine: spine,
    sprite: sprite,
    font: font,
    audio: audio,
    prefab: prefab,
    preload: [
        { url: json.dbJson, type: cc.JsonAsset },

        // // audio
        // { url: dir.audio, type: cc.AudioClip, isDir: true },

        { url: prefab.MainUI, type: cc.Prefab },
        { url: prefab.VideoUI, type: cc.Prefab },
        { url: prefab.WaitUI, type: cc.Prefab },
    ],
    preloadLate: [

    ]
};

export enum UI {
    MAIN = 'MAIN',
    VIDEO = 'VIDEO',
    WAIT = 'WAIT',

};

export const UI_RES: Record<UI, VIEW_INTER> = {
    MAIN: { url: prefab.MainUI, tsName: "MainUI", type: cc.Prefab, viewType: VIEW_TYPE.UI },
    VIDEO: { url: prefab.VideoUI, tsName: "VideoUI", type: cc.Prefab, viewType: VIEW_TYPE.DIALOG },
    WAIT: { url: prefab.WaitUI, tsName: "WaitUI", type: cc.Prefab, viewType: VIEW_TYPE.TIPS },
};