import BaseComponent from "../base/BaseComponent";
import { SEARCH_TYPE } from "../config/Enums";
import { DB_INFO_INTER } from "../config/Interface";

const { ccclass, property } = cc._decorator;

@ccclass
export default class SearchItem extends BaseComponent {

    @property(cc.Node)
    sprIcon: cc.Node = null;

    @property(cc.Label)
    labName: cc.Label = null;

    @property(cc.Node)
    nodeSign: cc.Node = null;
    @property(cc.Node)
    btnSign: cc.Node = null;

    private signList: cc.Node[] = [];

    private curData: DB_INFO_INTER;

    onLoad() {
        super.onLoad();
        this.init();
    }

    init() {
        this.onClick(this.node, this.onClickItem, this);
    }

    addEventListener() {
    }

    setData(data: DB_INFO_INTER) {
        this.curData = data;
        app.resMgr.loadRemote(`${app.cons.comCfg.serverUrl}${this.curData.icon}`, (err:Error, asset:cc.Texture2D)=>{
            this.sprIcon.getComponent(cc.Sprite).spriteFrame = new cc.SpriteFrame(asset);
        });
        this.labName.string = app.stringUtil.cutStr(data.name || ``);
        this.updateList();
    }

    updateList() {
        let list = this.curData.sign || [];
        app.nodeMgr.addListItemSelf(list.length, this.signList, this.btnSign, this.nodeSign, (item: cc.Node, idx: number) => {
            item.getComponent(cc.Label).string = list[idx];
            this.onClick(item, () => {
                app.ctrl.search(list[idx], SEARCH_TYPE.SIGN);
            });
        });
    }

    onClickItem() {
        console.log('SearchItem >>>>>>>> 57', this.curData);
    }

}