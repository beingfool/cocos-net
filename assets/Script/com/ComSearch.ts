import BaseComponent from "../base/BaseComponent";
import { SEARCH_TYPE } from "../config/Enums";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ComSearch extends BaseComponent {

    @property(cc.EditBox)
    editSearch: cc.EditBox = null;

    @property(cc.Node)
    btnSearch: cc.Node = null;

    @property(cc.Node)
    nodeHistory: cc.Node = null;
    @property(cc.Node)
    btnHistory: cc.Node = null;

    private historyList:cc.Node[] = [];

    onLoad() {
        super.onLoad();
        this.init();
    }

    init() {
        this.onClick(this.btnSearch, this.onClickSearch, this);

        this.updateList();
    }

    addEventListener() {
    }

    updateList(){
        // let count = 10;
        let list = app.ctrl.getHistory();
        app.nodeMgr.addListItemSelf(list.length, this.historyList, this.btnHistory, this.nodeHistory, (item:cc.Node, idx:number)=>{
            let info = list[idx];
            item.getComponent(cc.Label).string = info.search;
            this.onClick(item, ()=>{
                this.editSearch.string = info.search;
                app.ctrl.search(info.search, info.type);
            });
        });
    }

    onClickSearch() {
        app.ctrl.search(this.editSearch.string, SEARCH_TYPE.MAIN);
    }

}