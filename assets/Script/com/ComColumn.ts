import { EventType } from "../../../coco-mat/lib/CCMVideo/CCMVideo";
import BaseComponent from "../base/BaseComponent";
import { VIDEO_CB_TYPE } from "../config/Enums";
import { CCMVideoSlider } from "./CCMVideoSlider";

const { ccclass, property } = cc._decorator;

@ccclass
export default class ComColumn extends BaseComponent {

    @property(cc.Node)
    btnPrevious: cc.Node = null;

    @property(cc.Node)
    btnPlay: cc.Node = null;
    @property(cc.Node)
    sprPlay: cc.Node = null;
    @property(cc.Node)
    sprPause: cc.Node = null;

    @property(cc.Node)
    btnNext: cc.Node = null;

    @property(CCMVideoSlider)
    videoSlider: CCMVideoSlider = null;

    @property(cc.Label)
    labTime: cc.Label = null;

    @property(cc.Node)
    btnSpeed: cc.Node = null;
    @property(cc.Node)
    nodeSpeed: cc.Node = null;
    @property(cc.Node)
    btnRate: cc.Node = null;

    @property(cc.Node)
    btnFull: cc.Node = null;

    private speedList: cc.Node[] = [];
    // private curVideo: CCMVideo;
    private clickCb?: (status: VIDEO_CB_TYPE, data?: any) => void;

    onLoad() {
        super.onLoad();
        this.init();
    }

    init() {
        this.onClick(this.btnPrevious, this.onClickPrevious, this);
        this.onClick(this.btnPlay, this.onClickPlay, this);
        this.onClick(this.btnNext, this.onClickNext, this);
        this.onClick(this.btnSpeed, this.onClickSpeed, this);
        this.onClick(this.btnFull, this.onClickFull, this);
    }

    addEventListener() {
    }

    setData(callback?: (status: VIDEO_CB_TYPE, data?: any) => void) {
        // this.curVideo = video;
        this.clickCb = callback;
    }

    updateState(state:EventType){
        this.sprPause.active = state == EventType.PLAYING;
        this.sprPlay.active = state != EventType.PLAYING;
    }

    updateList() {
        let list = [0.5, 0.75, 1, 1.25, 1.5, 1.75, 2, 2.5, 3];
        app.nodeMgr.addListItemSelf(list.length, this.speedList, this.btnRate, this.nodeSpeed, (item: cc.Node, idx: number) => {
            this.onClick(item, () => {
                this.clickCb && this.clickCb(VIDEO_CB_TYPE.SPEED, list[idx]);
                this.nodeSpeed.active = false;
            });
        });
    }

    onClickPrevious() {
        this.clickCb && this.clickCb(VIDEO_CB_TYPE.PREVIOUS);
    }

    onClickPlay() {
        if(this.sprPlay.active){
            this.clickCb && this.clickCb(VIDEO_CB_TYPE.PLAY);
        }else{
            this.clickCb && this.clickCb(VIDEO_CB_TYPE.PAUSE);
        }
    }

    onClickNext() {
        this.clickCb && this.clickCb(VIDEO_CB_TYPE.NEXT);
    }

    onClickSpeed() {
        this.nodeSpeed.active = !this.nodeSpeed.active;
    }

    onClickFull() {
        this.clickCb && this.clickCb(VIDEO_CB_TYPE.FULL);
    }

    updateCurrentTime(progress: number, totalTime: number) {
        let currentTime = totalTime * progress;
        let minutes = Math.floor(currentTime / 60);
        let seconds = Math.floor(currentTime) % 60;
        let minutePrefix = minutes < 10 ? '0' : '';
        let secondPrefix = seconds < 10 ? '0' : '';
        this.videoSlider.progress = progress;
        this.labTime.string = `${minutePrefix}${minutes}:${secondPrefix}${seconds}`;
    }

    private onSlide() {
        this.clickCb && this.clickCb(VIDEO_CB_TYPE.TIME, this.videoSlider.progress);
    }

}