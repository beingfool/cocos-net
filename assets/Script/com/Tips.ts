import BaseComponent from "../base/BaseComponent";

const { ccclass, property } = cc._decorator;

@ccclass
export default class Tips extends BaseComponent {

    @property(cc.Node)
    nodeTips: cc.Node = null;

    private tipsPool:cc.NodePool = new cc.NodePool();

    public show(content: string, data: {position?: cc.Vec2, time?: number }) {
        let item:cc.Node = this.tipsPool.get() || cc.instantiate(this.nodeTips);
        item.active = true;
        cc.Tween.stopAllByTarget(item);
        item.y = 0;
        item.opacity = 255;
        item.parent = this.node;
        let labTips = this.seekChild(item, "lblTips");
        labTips.getComponent(cc.Label).string = content;
        let time = data.time || 2;
        cc.tween(item).to(time * 0.4, {y:150}).to(time * 0.6, {y:250, opacity:0}).call(()=>{
            this.tipsPool.put(item);
        }).start();
    }
}