import BaseComponent from "../base/BaseComponent";
import { SEARCH_TYPE } from "../config/Enums";

const { ccclass, property } = cc._decorator;

@ccclass
export default class TypeItem extends BaseComponent {

    @property(cc.Label)
    labType: cc.Label = null;

    @property(cc.Node)
    layout: cc.Node = null;
    @property(cc.Node)
    btnType: cc.Node = null;

    private typeList: cc.Node[] = [];
    private seleCallBack:Function;

    init(): void {
        this.layout.active = false;
        this.onClick(this.node, this.onLayerShow, this);
    }

    setData(info: { name: string, list: string[] }, callback:Function) {
        // this.labType.string = info.name;
        let list = info.list || [];
        app.nodeMgr.addListItemSelf(list.length, this.typeList, this.btnType, this.layout, (item: cc.Node, idx: number) => {
            // item.getComponent(cc.Label).string = list[idx];
            this.onClick(item, () => {
                app.ctrl.search(list[idx], SEARCH_TYPE.TYPE);
                this.onLayerShow();
            });
        });
        this.seleCallBack = callback;
    }

    onLayerShow(){
        this.setLayerVisible(!this.layout.active)
        this.seleCallBack && this.seleCallBack(this.layout.active);
    }

    setLayerVisible(visible:boolean){
        this.layout.active = visible;
    }

}