export default class DateUtil {

    // 获取某月某日是一年中的第几天
    static getDay(month?: number, day?: number): number {
        let date = new Date();
        const currentYear = date.getFullYear();
        if (month == null) {
            month = date.getMonth() + 1;
        }
        if (day == null) {
            day = date.getDate();
        }
        // 今天减今年的第一天（xxxx年01月01日）
        const hasTimestamp = new Date(currentYear, month - 1, day).getTime() - new Date(currentYear, 0, 1).getTime();
        let hasDays = Math.ceil(hasTimestamp / (24 * 60 * 60 * 1000));
        return hasDays + 1;
    }

    // 获取一年中的第几天是某月某日
    static getDateByDay(day: number = 1): { year: number, month: number, day: number } {
        let date = new Date();
        const currentYear = date.getFullYear();
        let time = new Date(currentYear, 0, 1).getTime() + (day - 1) * (24 * 60 * 60 * 1000);
        let changeData = new Date(time);
        return { year: currentYear, month: changeData.getMonth() + 1, day: changeData.getDate() };
    }

    // 时间段转 00:00:00格式
    public static formatSecToStr(seconds: number = 0) {
        let second = seconds % 60;
        let minute = Math.floor(seconds % (60 * 60) / 60);
        let hour = Math.floor(seconds / 60 / 60);
        let str = ``;
        if (hour > 0) {
            str = `${hour}:`
        }
        str = `${str}${minute.toString().padStart(2, '0')}:${second.toString().padStart(2, '0')}`
        return str;
    }

    // 时间段转 `yyyy/MM/dd/HH/mm/ss` 格式
    public static formatTime(time: number, format: string) {
        let tf = (i: number) => { return (i < 10 ? '0' : '') + i };
        let curTime = Math.ceil(time);
        return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
            let mult = 0;
            switch (a) {
                case 'yyyy':
                    mult = 365 * 24 * 60 * 60; break;
                case 'MM':
                    mult = 30 * 24 * 60 * 60; break;
                case 'dd':
                    mult = 24 * 60 * 60; break;
                case 'HH':
                    mult = 60 * 60; break;
                case 'mm':
                    mult = 60; break;
                case 'ss':
                    mult = 1; break;
            }
            if (mult > 0) {
                let num = Math.floor(curTime / mult);
                curTime -= num * mult;
                return tf(num);
            }
            return a;
        });
    };

    // 时间点转 `yyyy/MM/dd/HH/mm/ss` 格式
    public static formatDateTime(time: number, format: string) {
        let t = new Date(time);
        let tf = (i: number) => { return (i < 10 ? '0' : '') + i };
        return format.replace(/yyyy|MM|dd|HH|mm|ss/g, function (a) {
            switch (a) {
                case 'yyyy':
                    return tf(t.getFullYear());
                case 'MM':
                    return tf(t.getMonth() + 1);
                case 'mm':
                    return tf(t.getMinutes());
                case 'dd':
                    return tf(t.getDate());
                case 'HH':
                    return tf(t.getHours());
                case 'ss':
                    return tf(t.getSeconds());
            }
        })
    };



}