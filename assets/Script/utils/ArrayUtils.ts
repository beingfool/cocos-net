import MathUtil from "./MathUtil";

export default class ArrayUtil {

    //delArea 范围删除
    public static randByList(list: number[], isSplice: boolean = true, delArea: number = 0): number {
        let index = Math.floor(Math.random() * list.length);
        if (isSplice) {
            let idx = index - delArea;
            let delNum = delArea * 2 + 1
            if (idx < 0) {
                idx = 0;
            } else if (idx > list.length - delNum) {
                idx = list.length - delNum
            }
            let randIdx = list.splice(idx, delNum);
            return randIdx[Math.floor((randIdx.length - 1) / 2)];
        } else {
            return list[index];
        }
    }

    public static randList(list: any[], num?: number) {
        list.sort(() => { return Math.random() - 0.5 });
        if (num) {
            return list.slice(0, num);
        }
        return list;
    }

    /**
     * 
     * @param arr 数组乱序
     */
    public static shuffle(arr: any[]) {
        if (arr == null || arr.length == 0)
            return arr;
        let length = arr.length;
        for (let i = 0; i < length; i++) {
            let j = MathUtil.randomRangeInt(0, length)
            let temp = arr[i];
            arr[i] = arr[j]
            arr[j] = temp;
        }
        return arr;
    }

    /**
     * 
     * @param arr 浅拷贝
     */
    public static clone<T>(arr: T[]) {
        let ary: T[] = []
        arr.forEach((value, index) => {
            ary.push(value)
        })
        return ary;
    }

    /**深拷贝 */
    public static deepClone(obj) {
        let _obj = JSON.stringify(obj),
            objClone = JSON.parse(_obj);
        return objClone
    }
}