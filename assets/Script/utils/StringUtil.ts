export default class StringUtil {

    /**
    * 过滤掉特殊符号
    * @param {string} str 
    */
    static filterStr(str: string) {
        let pattern = new RegExp("[`~!@#$^&*()=|{}':;',\\[\\].<>/?~！@#￥……&*（）——|{}【】‘；：”“'。，、？%+_]");
        let newStr = '';
        for (let i = 0; i < str.length; i++) {
            newStr += str.substring(i, i + 1).replace(pattern, '');
        }
        return newStr;
    }

    /**
    * 去掉前后空格
    * @param {string} str
    * @returns {string}
    */
    static trimSpace(str: string): string {
        return str.replace(/^\s*(.*?)[\s\n]*$/g, '$1');
    }

    /**
     * 获取字符串长度，中文为2
     * @param {string} str
     * @returns {number}
     */
    static getLength(str: string): number {
        const strArr: Array<string> = str.split("");
        let length: number = 0;
        let indexStr: string;
        for (let i: number = 0, len: number = strArr.length; i < len; i++) {
            indexStr = strArr[i];
            if (this.isChinese(indexStr)) {
                length += 2;
            }
            else {
                length += 1;
            }
        }
        return length;
    }

    /**
     * 判断一个字符串是否包含中文
     * @param {string} str
     * @returns {boolean}
     */
    static isChinese(str: string): boolean {
        let reg: RegExp = /^.*[\u4E00-\u9FA5]+.*$/;
        return reg.test(str);
    }

    /**
     * 阿拉伯数字转中文
     * @param num 
     * @returns 
     */
    public static toChineseBig(num: number) {
        var arr1 = ['零', '一', '二', '三', '四', '五', '六', '七', '八', '九']
        var arr2 = ['', '十', '百', '千', '万', '十', '百', '千', '亿', '十', '百', '千', '万', '十', '百', '千', '亿']
        if (!num || isNaN(num)) return '零'
        var english = num.toString().split('')
        var result = ''
        for (var i = 0; i < english.length; i++) {
            var des_i = english.length - 1 - i// 倒序排列设值
            result = arr2[i] + result
            var arr1_index = english[des_i]
            result = arr1[arr1_index] + result
        }
        result = result.replace(/零(千|百|十)/g, '零').replace(/十零/g, '十') // 将【零千、零百】换成【零】 【十零】换成【十】
        result = result.replace(/零+/g, '零') // 合并中间多个零为一个零
        result = result.replace(/零亿/g, '亿').replace(/零万/g, '万') // 将【零亿】换成【亿】【零万】换成【万】
        result = result.replace(/亿万/g, '亿') // 将【亿万】换成【亿】
        result = result.replace(/零+$/, '') // 移除末尾的零
        result = result.replace(/^一十/g, '十') // 将头部的【一十】换成【十】
        return result
    }

    /**
    * @param str 要截取的字符串
    * @param Len 要截取的字节长度，注意是字节不是字符，一个汉字两个字节
    * @return 截取后的字符串
    */
    static cutStr(str: string, Len: number = 14): string {
        let result = '';
        let strlen = str.length; // 字符串长度
        let chrlen = str.replace(/[^\x00-\xff]/g, '**').length; // 字节长度

        if (chrlen <= Len) { return str; }

        for (var i = 0, j = 0; i < strlen; i++) {
            var chr = str.charAt(i);
            if (/[\x00-\xff]/.test(chr)) {
                j++; // ascii码为0-255，一个字符就是一个字节的长度
            } else {
                j += 2; // ascii码为0-255以外，一个字符就是两个字节的长度
            }
            if (j <= Len) { // 当加上当前字符以后，如果总字节长度小于等于Len，则将当前字符真实的+在result后
                result += chr;
            } else { // 反之则说明result已经是不拆分字符的情况下最接近Len的值了，直接返回
                return result;
            }
        }
    }

}