
export default class Utils {

    /**
     * 获取存储数据
     * @param key 键
     * @param value 默认值
     */
    public static getItem<T>(key: string, value?: T): any {
        const item: any = cc.sys.localStorage.getItem(key);
        if (item !== null && item !== "") {
            if (value != null && typeof (value) == 'object') {
                try {
                    let data = JSON.parse(item);
                    if (data.constructor !== value.constructor) {
                        return value;
                    }
                    return data;
                } catch (error) {
                    console.error(`getItem JSON ${key}, error : ${error}, `, item);
                    return value;
                }
            } else {
                if (value != null) {
                    if (typeof (value) == 'number') {
                        return Number(item);
                    } else if (typeof (value) == "boolean") {
                        return "true" == item;
                    }
                }
                return item;
            }
        } else {
            return value;
        }
    }

    /**
     * 保存存储数据
     * @param key 键
     * @param value 值
     */
    public static setItem(key: string, value: any): void {
        if (typeof (value) == 'object') {
            cc.sys.localStorage.setItem(key, JSON.stringify(value));
        } else {
            cc.sys.localStorage.setItem(key, value);
        }
    }


    /**
     * 深拷贝一个新的map
     * @any obj 
     */
    public static clone(obj: any) {
        let value;
        if (obj && obj.constructor === Object) {
            value = new obj.constructor();
            for (const key of Object.keys(obj)) {
                value[key] = this.clone(obj[key]);
            }
        } else if (obj && obj.constructor === Array) {
            value = [];
            for (let i = 0; i < obj.length; i++) {
                value.push(this.clone(obj[i]));
            }
        } else {
            value = obj;
        }
        return value;
    }

    //分帧执行
    private static executePreFrame(generator: Generator, executeFinish?: Function, duration: number = 1 / 60) {
        return new Promise((resolve, reject) => {
            let gen = generator;
            // 创建执行函数
            let execute = () => {
                // 执行之前，先记录开始时间
                let startTime = new Date().getTime();
                //执行过程中所在节点被销毁
                if (executeFinish && executeFinish()) {
                    resolve(false);
                    return;
                }
                // 然后一直从 Generator 中获取已经拆分好的代码段出来执行
                for (let iter = gen.next(); ; iter = gen.next()) {
                    // 判断是否已经执行完所有 Generator 的小代码段，如果是的话，那么就表示任务完成
                    if (iter == null || iter.done) {
                        //@ts-ignore
                        resolve(true);
                        return;
                    }

                    // 每执行完一段小代码段，都检查一下是否已经超过我们分配的本帧，这些小代码端的最大可执行时间
                    if (new Date().getTime() - startTime > duration) {
                        // 如果超过了，那么本帧就不在执行，开定时器，让下一帧再执行
                        setTimeout(() => {
                            execute();
                        }, duration * 1000);
                        return;
                    }
                }
            };

            // 运行执行函数
            execute();
        });
    }

    // 分帧回调
    public static async executeFrame(length: number, executeCb: Function, executeFinish?: Function, duration: number = 1 / 60) {
        await this.executePreFrame(this.getItemGenerator(length, executeCb), executeFinish, duration);
        // executeFinish && executeFinish();
    }

    private static *getItemGenerator(length: number, callback: Function) {
        for (let i = 0; i < length; i++) {
            yield callback(i);
        }
    }

    /**
     * 数组必须是按结构来的，会根据权重随机返回一个值
     * @param array 结构:
     */
    public static getIndexByWeight(array: any[]): number {
        let totalWeight = 0;
        for (let index = 0; index < array.length; index++) {
            let weight = array[index].weight || 1;
            totalWeight += weight;
        }
        let target = Math.random() * totalWeight;
        for (let index = 0; index < array.length; index++) {
            let weight = array[index].weight || 1;
            totalWeight -= weight;
            if (target >= totalWeight) {
                return index;
            }
        }
        return 0;
    }

    public static getListByWeight(list: any[] = [], num: number = 1) {
        let copyList = this.clone(list);
        let idxs = [];
        for (let i = 0; i < num; i++) {
            let idx = this.getIndexByWeight(copyList);
            let reward = copyList.splice(idx, 1);
            idxs = idxs.concat(reward);
        }
        return idxs;
    }
}