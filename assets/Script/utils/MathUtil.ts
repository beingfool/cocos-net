
export default class MathUtil {

    public static randByArea(area: number[], isFC: boolean = false) {
        if (area == null || area.length < 2) return 0;
        let rand = Math.random() * (area[1] - area[0]) + area[0];
        if (isFC) {
            return Math.floor(rand);
        }
        return rand;
    }

    static randomRangeInt(min: number, max: number): number {
        let rand = Math.random();
        return min + Math.floor(rand * (max - min));
    }

    static randomRangeFloat(min: number, max: number): number {
        return min + (Math.random() * (max - min));
    }

    /**获取两点间弧度 */
    public static getRadianTwoPoint(p1: cc.Vec2 | cc.Vec3, p2: cc.Vec2 | cc.Vec3): number {
        const xdis: number = p2.x - p1.x;
        const ydis: number = p2.y - p1.y;
        return Math.atan2(ydis, xdis);
    }

    /**获取两点间角度 */
    public static getAngelTwoPoint(p1: cc.Vec2 | cc.Vec3, p2: cc.Vec2 | cc.Vec3): number {
        let radian = this.getRadianTwoPoint(p1, p2);
        return this.getAngel(radian);
    }


    /**弧度值转换为角度 */
    public static getAngel(radian: number): number {
        return radian * 180 / Math.PI;
    }


    /**角度值转换为弧度制 */
    public static getRadian(angle: number): number {
        return angle / 180 * Math.PI;
    }

    /** 圆心，半径，角度，求圆上的点坐标 */
    public static getCirclePos(pos: cc.Vec2, radius: number, angle: number): cc.Vec2 {
        const x = pos.x + radius * Math.cos(angle * Math.PI / 180);
        const y = pos.y + radius * Math.sin(angle * Math.PI / 180);
        return cc.v2(x, y);
    }

}
