import Singleton from "../base/Singleton";

export default class Timer extends Singleton {

    public scheduleOnce(callback: Function, delay?: number, target?: any) {
        this.schedule(callback, 0, 0, delay, target);
    }

    public schedule(callback: Function, interval?: number, repeat?: number, delay?: number, target?: any) {
        target = target || this;
        cc.director.getScheduler().schedule(callback, target, interval, repeat, delay, false);
    }

}